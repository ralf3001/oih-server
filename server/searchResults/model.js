var mongoose = require('mongoose');
var User = require('../models/user');
var connection = require('../../config/userDBConnection');

var searchResultsSchema = mongoose.Schema({

  	user: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
	createdAt: {type: Date, default: Date.now},
	purchaseTime: {type: Date},
  	searchedStores: [{type: mongoose.Schema.Types.ObjectId, ref: 'Restaurant'}],
  	searchedMeals: [{type: mongoose.Schema.Types.ObjectId, ref: 'Meal'}],
  	purchasedItem: {type: mongoose.Schema.Types.ObjectId, ref: 'Meal'},
  	surchargeRate: {type: Number}

});


var SearchResultModel = connection.model('searchResult', searchResultsSchema);
module.exports = SearchResultModel;

