// load the things we need
var mongoose = require('mongoose');
var bcrypt   = require('bcrypt');
var userConnection = require('../../config/userDBConnection'),
    jwt = require("jsonwebtoken");


var emailExp = '^[A-Z0-9a-z\._%+-]+@([A-Za-z0-9-]+\.)+[A-Za-z]{2,4}$';

// define the schema for our user model
var userSchema = mongoose.Schema({

    local            : {
        email        : {type: String, unique: true},
        password     : String,
        select: false
    },

    profilePic: [String],
    address: [{type: mongoose.Schema.Types.ObjectId, ref: 'Address'}],

    phoneNumber		 : [{number: String, region: String, select: false}],
    accessToken      : String,
    brainTreeClientToken: [String],
    braintreeCustomerId: String,
    admin            : {type: Boolean, default: false, select: false},
    name             : String,
    lastName         : String,
    shoppingCart     : [{type: mongoose.Schema.Types.ObjectId, ref: 'Meal'}],
    activated        : {type: Boolean, select: false},
    gender			 : Number,	//0 male, 1 female
    orders           : [{quantity: Number, productID: String}],
    favorites        : [{quantity: Number, productID: String}],
    cards            : [{last4: String, expMonth: Number, expYear: Number, brand: String, stripeToken: String, customerPaymentProfileId: String, cardholderName: String}],
    userID           : Number,
    locations        : [{sessionTime: Number, sessionEndTime:{type: Date, default: Date.now}, latitude: Number, longitude: Number, postal: String}],
    merchantsOf      : [{type: mongoose.Schema.Types.ObjectId, ref: 'Restaurant'}],
    isMerchant       : {type: Boolean, default: false},
    
    allowToBeAddedAsFriend: {type: Boolean, default: true},
    friends			 : [{type: mongoose.Schema.Types.ObjectId, ref: 'User'}],
    friendRequests   : [{type: mongoose.Schema.Types.ObjectId, ref: 'User'}],
    pendingFriendRequests: [{type: mongoose.Schema.Types.ObjectId, ref: 'User'}],
    followers        : [{type: mongoose.Schema.Types.ObjectId, ref: 'User'}],
    following        : [{type: mongoose.Schema.Types.ObjectId, ref: 'User'}],
    
    lastLogin		 : {type: Date, default: Date.now},
    
    pushNotificationTokens: [{token: String, device: String}],

    socialTokens: [{socialNetwork: String, userId: String, access_token: String, expiry_date: Date}],
    favourites: [{type: mongoose.Schema.Types.ObjectId, ref: 'Meal'}],

    subscribing: [{type: mongoose.Schema.Types.ObjectId, ref: 'Subscription'}],

    resetPasswordToken: String,
    resetPasswordExpires: Date
});

//obfuscate credit card number
function obfuscate (cc) {
  return '****-****-****-' + cc.slice(cc.length-4, cc.length);
}

// generating a hash
userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.local.password);
};


userSchema.methods.generateRandomToken = function(){
    var user = this, 
      chars = "_!abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890",
      token = new Date().getTime() + '_';

    for ( var x = 0; x < 16; x++ ) {
        var i = Math.floor( Math.random() * 62 );
        token += chars.charAt( i );
    }
    
    token = jwt.sign(user, process.env.JWT_SECRET);

    return token;
};



// create the model for users and expose it to our app
var UserModel = userConnection.model('User', userSchema);
module.exports = UserModel;
