
var Restaurant = require('../restaurant/model'),
	assert = require('assert'),
	geocoder = require('geocoder'),
	Elo = require('arpad'),
	Meal = require('../meal/model'),
	https = require('https'),
	timezoner = require('timezoner'),
	async = require('async'),
	mongoose = require('mongoose'),
	tz = require('tz-lookup'),
	time = require('time'),
	User = require('../models/user'),
	Address = require('../address/model'),
	Activity = require('../activity/model'),
	Order = require('../order/model'),
	SearchResult = require('./model')



module.exports = function () {
	
	return {
		create: function(req, res, next){

			// lng, lat
			var body = req.query;
			var recipientUser = req.query.userId;

			async.waterfall([

				function(callback){

					if (recipientUser) {

						Address.find({user: recipientUser}).lean().exec(function(err, addresses){
							if (err) {callback(err)}
							if (addresses.length > 0) {
								var address = addresses[0];
								callback(null, address.geometry.coordinates);
							}else{
								callback(new Error("USER_HAS_NO_ADDRESS"));
							}
						});

					}else if(body.lng){
						//use user's current coordinates
						callback(null, [Number(body.lng), Number(body.lat)]);

					}else{
						//use user's address
						var self = req.user;
						if (self.address.length > 0) {

							var address = self.address[0];
							var coordinates = [];
							coordinates.push(address.lng);
							coordinates.push(address.lat);

							callback(null, coordinates);
						}else{
							callback(new Error("USER_HAS_NO_ADDRESS"));
						}


					}
				},
				function(userCoordinates, callback){

					// userCoordinates = [114.195984,22.397524];
					var timezone = tz(userCoordinates[1], userCoordinates[0]);
					time.tzset(timezone);
						
					var type = mealType(time.localtime(Date.now()/1000));
					//doesn't yet take into account restaurants don't open
					// var coordinates = req.query.coordinates.split(",").map(Number).filter(Boolean);;
					findingItems(userCoordinates, type, function(err, results){
						callback(err, results);
					});

				},
				function(restaurants, callback){

					var searchResults = new SearchResult()
					searchResults.surchargeRate = Number.random(1.07, 1.13, 2)
					searchResults.searchedStores = restaurants;
					searchResults.user = req.user.id

					var list = _.pluck(restaurants, 'latestItem')
					searchResults.searchedMeals = _.flatten(list)
					
					searchResults.save(function(err, result){
						callback(err, result)
					});

				},

				function(searchResult, callback){
					SearchResult.findById(searchResult.id)
								.lean()
								.populate('searchedMeals searchedStores', 'name restaurantName currency priceInLoca eloScore sellersNote restaurantPictures type priceInLocalCurrency priceInUSD priceInLocalCurrencyString productImage productIngredients productNetWeightInString productNote') 
								.exec(function(err, result){
									callback(err, result)
								});
				},

				function(searchResults, callback){

					async.each(searchResults.searchedStores, function(restaurant, cb){
						
						Order.find({sellerRestaurant: restaurant}).select('_id sellerRestaurant').lean().exec(function(err, orders){
							if (err) {return cb(err);}

							Activity.find({'activityType': 'review', 'order': {$in: orders}}).select('order rating').lean().exec(function(err, reviews){
								if (err) {console.log(err); return cb(err);}
								else{
									
									var scores = Array.apply(null, Array(5)).map(Number.prototype.valueOf,0);

									_.each(reviews, function(review){
										scores[review.rating]++;
									});

									restaurant.rating = scores;

									cb(null);
								}
							});
						});

					}, function(err){
						callback(err, searchResults);
					});
				}

			], function(err, results){
				if (err) {
					res.statusCode = 500
					res.send({"error": err.message});
				}else{
					res.send(results);	
				} 
				
			});
		},

		update: function(req, res, next){

			var id = req.body.searchId;

			SearchResult.findByIdAndUpdate(id, {$set:{purchasedItem: req.meal.id}}).populate('searchedStores searchedMeals','restaurant eloScore').exec(function(err, result){
				if (err) {next(err);}
				else{
					result.purchasedItem = req.meal;
					req.restaurants = result.searchedStores;
					next();			
				}
			});

		}

	}

}

function calculateScores(restaurants, winnerRestaurant, cb){
	
	if (restaurants.length <= 1) {return cb(null, restaurants);}

	var restaurantsCopy = [];

	for (var i = 0; i < restaurants.length; i++) {

		var a = {};

		if (restaurants[i].id === winnerRestaurant.id) {
			a.place = 1;
		}else{
			a.place = 2;
		}

		a.eloPost = 0;
		a.eloChange = 0;
		a.eloPre = restaurants[i].eloScore;
		a._id = restaurants[i]._id;
		restaurantsCopy.push(a);

	}
	
	process.nextTick(function(){
	
		var n = restaurants.length;
		var change = Array.apply(null, Array(n)).map(Number.prototype.valueOf,0);
		var K = 32 / (n - 1);

		for (var i = 0; i < n; i++) {

			var myElo = restaurantsCopy[i].eloPre;
			var place = restaurantsCopy[i].place;
			for (var j = 0; j < n; j++){
				if (i != j) {

					var opponentPlace = restaurantsCopy[j].place;
					var opponentElo = restaurantsCopy[j].eloPre
					if (place < opponentPlace){
						S = 1;
					}else if(place == opponentPlace){
						S = 0.5;
					}else
						S = 0;

					var EA = 1 / (1.0 + Math.pow(10.0, (opponentElo - myElo) / 400.0));
					restaurantsCopy[i].eloChange += Math.round(K * (S - EA));
				}
			}

			restaurantsCopy[i].eloPost = restaurantsCopy[i].eloPre + restaurantsCopy[i].eloChange;

		}
	
		cb(null, restaurantsCopy);

	});

}




function findingItems(coordinates, type, callback){


	Restaurant.aggregate([
			{

				$geoNear: {
					near: {type: "Point", coordinates: coordinates},
					includeLocs: "dist.geometry",
					distanceField: "dist.calculated",
					maxDistance: process.env.searchRadius,
					spherical: true

				}
			},
			{
				$lookup: {
					from: "meals",
					foreignField: 'restaurant',
					localField: '_id',
					as: 'm'
				}
			},
			
			// this part is for fetching food
			{
				$project:{
					m: {
						$filter: {
							input: '$m',
							as: 'm',
							cond: {$eq: ['$$m.type', type]}
						}
					},
					restaurantName: 1,
					currency: 1,
					eloScore: 1,
					sellersNote: 1,
					restaurantPictures: 1

				}
			},
			{
				$project:{
					latestItem :{
						$slice: ['$m', 1]
					},
					restaurantName: 1,
					currency: 1,
					eloScore: 1,
					sellersNote: 1,
					restaurantPictures: 1
				}
			},
			{
				$sort: {
					'm.availableDate':-1
				}
			},

			{
				$project: {
					latestItemSize: {$size: "$latestItem"},
					restaurantName: 1,
					currency: 1,
					eloScore: 1,
					sellersNote: 1,
					restaurantPictures: 1,
					latestItem: 1
				}
			},
			{
				$match: {
					latestItemSize: {$gte: 1}
				}
			},
			{
				$sort: {
					eloScore: -1
				}
			},
			{
				$limit: 6
			}
			], function(err, results){
				callback(err, results);
			});

}



Number.random = function(minimum, maximum, precision) {
    minimum = minimum === undefined ? 0 : minimum;
    maximum = maximum === undefined ? 9007199254740992 : maximum;
    precision = precision === undefined ? 0 : precision;

    var random = Math.random() * (maximum - minimum) + minimum;

    return random.toFixed(precision);
}
