var User = require('../models/user'),
	_ = require('underscore'),
	geocoder = require('geocoder'),
	async = require('async'),
	Address = require('../address/model'),
	utf8 = require('utf8')

module.exports = function() {

	return {
		delete: function(req, res, next){

			Address.findByIdAndRemove(req.query.id, function(err){
				if (err) {next(err);}
				else{
					res.sendStatus(200);
				}
			});

		},
		addAddress: function(req, res, next){

			var body = req.body;
			var address = "";
			var user = req.user;

			if (body.apt) {address += body.apt; address += ' ';}
			address += body.street;
			address += ", " + body.city;
			if (body.state) address += ", " + body.state;

		    geocoder.selectProvider("google");
			geocoder.geocode(address, function(err, data){
				if (err) {callback(err);}
				else {

					if (data.results.length != 0) {

						async.waterfall([
							function(cb){


								var addressObj = new Address();
								addressObj.userEnteredAddress = (address);
								addressObj.formattedAddress = data.results[0].formatted_address;
								addressObj.addressComponens = data.results[0];

								var postCode = findKey('postal_code', data.results[0].address_components);
								if (postCode) {
									addressObj.postalCode = postCode;
								}

								var coordinates = [data.results[0].geometry.location.lng, data.results[0].geometry.location.lat];
								addressObj.geometry.coordinates = coordinates;
								addressObj.geometry.type = 'Point';
								addressObj.user = req.user;

								addressObj.tag = req.body.tag;
								addressObj.save(function(err, a){
									cb(err, a);
								});

							},
							function(addressObj, cb){
								user.address.push(addressObj);
								user.save(function(err){
									cb(err);
								});

							}

							], function(err){

								if (err) {next(err)}
								else res.sendStatus(200);

						});
			
					}else{
						res.status = 500;
						res.send({"error": "Address cannot be verified"});
					}

				}
			}, {language: req.body.language});					


		},
		fetchAddress: function(req, res, next){
			var selectString = '';

			if (req.query.userId !== req.user._id.toString()) {
				selectString = '_id';
			}else{
				selectString = '_id userEnteredAddress formattedAddress tag';
			}

			Address.find({user: req.query.userId}).lean().select(selectString).exec(function(err, addresses){
				if (err) {next(err);}
				else{
					res.send(addresses);					
				}
			});

		},
		test: function(req, res, next){

			var body = req.body;
			var address = "";
			var user = req.user;

			if (body.apt) {address += body.apt; address += ' ';}
			address += body.street;
			address += ", " + body.city;
			if (body.state) address += ", " + body.state;
		    geocoder.selectProvider("google");
			geocoder.geocode(address, function(err, data){
				if (err) {next(err);}
				else {

					if (data.results.length != 0) {
						console.log(findKey('postal_code', data.results[0].address_components));
					}
					next();
				}
			});

		}

	}
}


function findKey (key, components) {

	var matches = components.filter(function(x){
		return _.intersection([key], x.types).length > 0;
	});
	return matches[0].long_name;
}



