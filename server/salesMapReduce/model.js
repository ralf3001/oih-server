var mongoose = require('mongoose');
var connection = require('../../config/userDBConnection');

var monthlySalesSchema = mongoose.Schema({

	_id: {type: mongoose.Schema.ObjectId, ref: 'Restaurant'},
	value: {type: Number}

});


var salesModel = connection.model('SalesSchema', monthlySalesSchema);
module.exports = salesModel;

