var mongoose = require('mongoose');
var User = require('../models/user');
var connection = require('../../config/userDBConnection');

var giftcodeSchema = mongoose.Schema({

	createdDate: {type: Date, default: Date.now},
	usedDate: {type: Date},
	giftCodeId: {type: String, unique: true},
	user: {type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true}
});

giftcodeSchema.methods.generateGiftId = function(username) {
    
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    var text = username.replace(/\s+/g, '').toUpperCase().substring(0, 4);
    
    for( var i = 0; i < 5; i++ )
	    text += possible.charAt(Math.floor(Math.random() * possible.length));

	return text;
};


var giftcodeModel = connection.model('Coupon', giftcodeSchema);
module.exports = giftcodeModel;