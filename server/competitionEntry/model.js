var mongoose = require('mongoose'),
	connection = require('../../config/userDBConnection'),
	user = require('../models/user')

var competitionSchema = mongoose.Schema({

	geometry: {
		type: {type: String},
		coordinates: []
	},
	competition: {type: mongoose.Schema.Types.ObjectId, ref: 'Competition'},
	imageURL: {type: String, required: true},
	content: {type: String, required: true},
	createdAt: {type: Date, default: Date.now},
  	user: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
  	voteCount: {type: Number, default: 0}

});

competitionSchema.index({geometry: '2dsphere'});


var orderModel = connection.model('CompetitionEntry', competitionSchema);
module.exports = orderModel