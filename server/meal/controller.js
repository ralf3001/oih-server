var Meal = require('./model'),
	Restaurant = require('../restaurant/model')
	_ = require('underscore'),
	timezoner = require('timezoner'),
	cc = require('currency-codes'),
	async = require('async')



module.exports = function () {
	
	return {

		meal: function(req, res, next, id){
			Meal.findById(id, function(err, meal){
				if (err) {next(err);}
				if (!meal) {return next(new Error('Failed to load meal'));}
				req.meal = meal;
				req.receipt.meal = meal;
				next();
			});
		},
		findById: function(req, res, next){
			Meal.findById(req.body.mealId, function(err, meal){
				if (err) {console.log("MEAL ERRRRRORRRRRRR", err); next(err);}
				if (!meal) {console.log("Fail to find meal"); next(new Error('Failed to load meal'));}
				req.meal = meal;
				next();
			});

		},
		create: function(req, res, next){

			createMeal(req.body.meal, function(err, meal){
				if (err) {next(err);}
				else{
					req.meal = meal;
					req.body.activityType = "newItem";
					next();
				}
			});

		},

		destroy: function(req, res){
			var meal = req.meal;
			meal.remove(function(err){
				if (err) {return res.status(500).json({error: 'Cannot delete meal'});}
				res.json(meal);
			});
		},
		show: function(req, res){
			res.send(req.meal);
		},
		convertToString: function(req, res){
			Meal.find({}, function(err, meals){

				async.each(meals, function(meal, callback){
					meal.priceInLocalCurrencyString = meal.priceInLocalCurrency.toString();
					meal.save(function(err){
						callback(err);
					});
				}, function(err){
					if (err) {console.log(err);}
					else{
						console.log('done');
					}
				});

			});
		},
		fetch: function(req, res, next){
			if (req.query.restaurant) {

				var oDate = req.query.availableDate ? req.query.availableDate : Date.now();
				var query = fetchMealQuery(req.query.restaurant, oDate, req.query.forward);							

				mealQuery(query, function(err, meals){
					if (err) {next(err);}
					else {console.log(meals); res.send(meals);}
				});

			}else{

				Restaurant.find({$or: [{accountsAssociatedTo: {$in: [req.user.id]}}, {administrator: {$in: [req.user.id]}}], isDestroyed: false}).lean().exec(function(err, restaurants){			
					if (err) {next(err);}
					else{

						var ids = _.pluck(restaurants, '_id');

						var oDate = req.query.availableDate ? req.query.availableDate : Date.now();
						var query = fetchMealQuery(ids, oDate, req.query.forward);							

						mealQuery(query, function(err, meals){
							if (err) {next(err);}
							else {res.send(meals);}
						});
					}
				});
			
			}
		}
	}
}


fetchMealQuery = function(ids, oDate, forward){

	var fetchDate = new Date(oDate);

	var y = fetchDate.getFullYear();
	var m = fetchDate.getMonth();
	var d = fetchDate.getDate();

	var query = {};

	if (forward == true) {
		var end = new Date(y, m, d + 20);
		query = {restaurant: {$in: ids}, $and:[{availableDate: {$lte: end}}, {availableDate: {$gte: oDate}}]};
	}else{
		var end = new Date(y, m, d - 20);
		query = {restaurant: {$in: ids}, $and:[{availableDate: {$gte: end}}, {availableDate: {$lte: oDate}}]};
	}

	return query;
}


mealQuery = function(query, callback){

	Meal.find(query).lean()
	.limit(10)
	.select('restaurant type name priceInLocalCurrency priceInLocalCurrencyString productQuantity productImage availableDate currencyCode')
	.populate('restaurant', 'currency')
	.sort({'availableDate':-1})
	.exec(function(err, meals){
		callback(err, meals);
	});

}

createMeal = function(item, cb){


	Restaurant.findById(item._id, function(err, restaurant){
		if (err) {next(err);}
		else{
			var meal = new Meal();
			
			var type;
			switch(item.mealType){
				case 0:
					type = 'breakfast';
					break;
				case 1:
					type = 'lunch';
					break;
				case 2:
					type = 'dinner';
					break;
				case 3:
					type = 'snack';
					break;
				default:
					type = 'breakfast';
					break;
			}
			meal.productImage = item.productImage;
			meal.availableDate = item.availableDate;
			meal.type = type;
			meal.name = item.name;
			meal.productIngredients = item.productIngredients;
			// meal.productNetWeight = item.productNetWeight;
			meal.productQuantity = item.productQuantity;
			meal.productNetWeightInString = item.productNetWeightInString;
			meal.productNote = item.productNote;
			meal.restaurant = restaurant;
			meal.priceInLocalCurrencyString = item.priceInLocalCurrency.toString();
			meal.currencyCode = restaurant.currency;
			
			meal.save(function(err, meal){
				cb(err, meal);
			});
		}
	});


}


