var Restaurant = require('./model'),
	_ = require('underscore'),
	geocoder = require('geocoder'),
	async = require('async'),
	braintree = require("braintree"),
	cc = require('currency-codes'),
	utf8 = require('utf8'),
	Order = require('./../order/model'),
	Activity = require('./../activity/model'),
	SalesSchema = require('./../salesMapReduce/model'),
	oxr = require("open-exchange-rates"),
	fx = require("money"),
	ExchangeRate = require('../exchange-rate/model'),
	User = require('./../models/user')



module.exports = function () {
	
	return {
		restaurant: function(req, res, next, id){
			Restaurant.findById(id, function(err, restaurant){
				if (err) {next(err);}
				if (!restaurant) {return next(new Error('Failed to load restaurant'));}
				req.restaurant = restaurant;
				next()
			});
		},
		create: function(req, res, next){

			var restaurant = new Restaurant(req.body);
			restaurant.accountsAssociatedTo.push(req.user);

			async.waterfall([

				function(callback){
					
					var body = req.body;

					var address; 

					address = body.addressString;

				    geocoder.selectProvider("google");
					geocoder.geocode(address, function(err, data){
						if (err) {callback(err);}
						else {

							var country = _.find(data.results[0].address_components, function(component){
								return _.contains(component.types, 'country');
							});

							var location = data.results[0].geometry.location;
							var locationArray = [location.lng, location.lat];
							restaurant.geometry.coordinates = locationArray;
							restaurant.geometry.type = 'Point';
							restaurant.address.country = country.long_name; 
							restaurant.addressString = data.results[0].formatted_address;
							restaurant.phoneNumber = body.phoneNumber;
							restaurant.userEnteredAddress = address;
							
							var currency = cc.country(country.long_name);
							restaurant.currency = currency[0].code;
							
							callback(null, restaurant);
						}
					});					

				}, function(rest, callback){

					rest.save(function(err, r){
						callback(err, r);
					});

				}, function(rest, callback){
					callback(null);
					//send email to user to confirm a new restaurant, and ask for more details, e.g. bank account
				}

			], function(err, result){
				if (err) {next(err);}
				else{
					res.sendStatus(200);
				}
			});

		
		},
		update: function(req, res, next){

			var restaurant = req.restaurant;

			if (req.body.restaurantPictures) {
				Restaurant.findByIdAndUpdate(req.restaurant.id, {$addToSet: {restaurantPictures: req.body.restaurantPictures}}, function(err){
					if (err) {next(err);}
					else next();
				});
			}else{
				restaurant = _.extend(restaurant, req.body.restaurant);

			    geocoder.selectProvider("google");
				geocoder.geocode(req.body.restaurant.addressString, function(err, data){
					if (err) {next(err);}
					else{

						var country = _.find(data.results[0].address_components, function(component){
							return _.contains(component.types, 'country');
						});

						var location = data.results[0].geometry.location;
						var locationArray = [location.lng, location.lat];
						restaurant.geometry.coordinates = locationArray;
						restaurant.geometry.type = 'Point';
						restaurant.address.country = country.long_name; 
			
						restaurant.save(function(err){
							if (err) {return res.status(500).json({error: 'Cannot update the restaurant'})}
							else{ 
								next();
							}
						});

					}

				});

			}


		},
		destroy: function(req, res){

			var rest = req.restaurant;
			rest.isDestroyed = true;
			rest.save(function(err){
				next(err);
			});

		},
		show: function(req, res){
			res.send(req.restaurant)
		},
		all: function(req, res){
			// var page = req.query.page ? req.query.page;
			var user = req.query.userId ? req.query.userId : req.user.id;
			Restaurant.find({$or: [{accountsAssociatedTo: {$in: [user]}}, {administrator: {$in: [user]}}], isDestroyed: false}).lean().exec(function(err, restaurants){			

				if (err) {next(err);}
				else{res.send(restaurants);}
			});

		},
		findById: function(req, res, next){

			Restaurant.findById(req.body.restaurant, function(err, restaurant){
				if (err) {next(err);}
				if (!restaurant) {return next(new Error('Failed to load restaurant'));}
				req.restaurant = restaurant;
				next()
			});

		},
		statistics: function(req, res, next){

			var restaurant = req.restaurants;

			var today = new Date();

			var y = req.query.year ? req.query.year : today.getFullYear();
			var m = req.query.month ? req.query.month : today.getMonth() ;

			var start = new Date(y, m, 1);

			var o = {};
			o.map = function(){
				emit(
						this.sellerRestaurant,
						
					{
						quantity: this.quantity,
						amount: this.orderAmount,
						currency: this.currency

					}
				)
			};

			o.reduce = reduceFunction;
			o.out = {replace: 'salesMapReduce'};

			if (restaurant.constructor === Array) {
				o.query = {orderDate:{$gte: start, $lt: new Date()}, sellerRestaurant: {$in: restaurant}};

			}else{
				o.query = {orderDate:{$gte: start, $lt: new Date()}, sellerRestaurant: restaurant};
			}

			async.parallel([
					function(callback){
						totalSales(o, function(err, result){
							callback(err, result);
						});//returns total sales of all restaurants in a month
					},
					function(callback){
						if (restaurant.constructor === Array) {
							callback(null);
						}else{
							dailySales(restaurant, function(err, result){
								callback(err, result);
							})
						}
							
					}

				], function(err, results){
					if(err){next(err);}
					else{
						var data = results[0];

						if (restaurant.constructor === Array) {
							res.send(data);
						}else{

							if (results[1].length > 0) {
								data[0].salesPeriod = results[1];
							}
							res.send(data);

						}
					}
			});

		},
		orderAmountToReceipt: function(req, res, next){

			Restaurant.find({}).exec(function(err, restaurants){
				if (err) {next(	err);}
				else{

					async.each(restaurants, function(restaurant, cb){

						Order.find({sellerRestaurant: restaurant._id})
						.populate('orderReceipt orderMeal', 'totalAmount priceInLocalCurrency')
						.exec(function(err, orders){

							if (orders.length > 0) {

								async.each(orders, function(order, callback){

									var quantity = order.quantity ? order.quantity : 1
									order.orderAmount = quantity * order.orderMeal.priceInLocalCurrency;
									// order.orderAmount = order.orderReceipt.totalAmount;
									order.save(function(err){
										if (err) {console.log(err);}
										else{
											callback();											
										}
									});

								}, function(err){
									cb(err);
								});

							}else{
								cb();
							}

						});

					}, function(err){
							if (!err) {
								console.log('done');
							}else{
								console.log('ouch')
							}
						});

				}



			});

		},
		fetchUserRestaurants: function(req, res, next){
			
			if (req.query.restaurantId) {
				req.restaurants = req.query.restaurantId;
				next();
			
			}else{

				Restaurant.find({"accountsAssociatedTo": {$in: [req.user.id]}})
				.lean()
				.exec(function(err, restaurants){
					if (err) {next(err);}
					else{
						var ids = _.pluck(restaurants, "_id");
						req.restaurants = ids;
						req.currency = restaurants[0].currency;
						next();
					}
				});

			}
		},
		restaurantRating: function(req, res, next){
			
		},
		userAdministerRestaurant: function(req, res, next){

			Restaurant.find({"_id": req.params.restaurantId, $or: [{accountsAssociatedTo: {$in: [req.user.id]}}, {administrator: {$in: [req.user.id]}}]}).lean().exec(function(err, restaurants){			
			// Restaurant.find({accountsAssociatedTo: {$in: [req.user.id]}}, function(err, restaurants){
				if (err) {next(err);}
				else if(restaurants.length > 0){ req.restaurants = restaurants[0]; next();}
				else{
					next(new Error("NO_RESTAURANT_FOUND"));
				}
			});

		},
		information: function(req, res, next){
			
			var restaurant = req.restaurant;
			restaurant = _.pick(restaurant, 'address', 'sellersNote', 'geometry', 'restaurantName', 'addressString', '_id')
			
			async.waterfall([
				function(callback){
					Order.find({sellerRestaurant: restaurant})
						.select('_id sellerRestaurant')
						.lean()
						.exec(function(err, orders){
							callback(err, orders);
						});
				},
				function(orders, callback){
					Activity.find({'activityType': 'review', 'order': {$in: orders}})
						.select('order rating')
						.lean()
						.exec(function(err, reviews){
							if (err) {console.log(err); callback(err);}
							else{
								var scores = Array.apply(null, Array(5)).map(Number.prototype.valueOf,0);
								_.each(reviews, function(review){
									scores[review.rating]++;
								});
								callback(null, scores);								
							}
						});
				},
			], function(err, scores){
				if (err) {next(err);}
				else{

					var rest = {};

					restaurant.rating = scores;
					rest.restaurant = restaurant;
					rest.user = req.restaurantOwner;
					res.send(rest)

				}

			});

		},
		user: function(req, res, next){

			User.find({_id: {$in: req.restaurant.accountsAssociatedTo}})
				.lean()
				.select('name lastName allowToBeAddedAsFriend profilePic geometry address')
				.exec(function(err, users){
				if (err){
					next(err);
				}else{
					if (users.length > 0) {
						req.restaurantOwner = users[0];					
					}
					next();
				}
			});

		},
		updateScores: function(req, res, next){

			async.waterfall([
				function(cb){
					calculateScores(req.restaurants, req.restaurant, function(err, restaurantsUpdated){
						if (err) {cb(err);}
						else{
							cb(null, restaurantsUpdated);
						}
					});
				},
				function(restaurantsUpdated, cb){
					
					if (restaurantsUpdated.length > 1) {
						async.each(restaurantsUpdated, function(restaurant, callback){
							Restaurant.findByIdAndUpdate(restaurant._id, {$set: {eloScore: restaurant.eloPost}}, function(err){
								callback(err);
							});
						}, function(err){
							cb(err);
						});						
					}else{
						cb(null);
					}
				}

				], function(err){
					if (err) {next(err);}
					else{
						res.sendStatus(200);
					}
					
			});
		},
		recentlyAdded: function(req, res, next){
			
			var coordinates = req.query.coordinates.split(",").map(Number).filter(Boolean);;

			Restaurant.aggregate([
			{

				$geoNear: {
					near: {type: "Point", coordinates: coordinates},
					includeLocs: "dist.geometry",
					distanceField: "dist.calculated",
					maxDistance: 3000,
					spherical: true

				}
			},
			{
				$project: {restaurantName: 1, userEnteredAddress: 1, geometry: 1, eloScore: 1}
			},
			{
				$lookup: {
					from: "meals",
					foreignField: 'restaurant',
					localField: '_id',
					as: 'm'
				}
			},
			/*
			this part is for fetching food
			{
				$filter: {
					input: "$m.type",
					cond: {""}
				}
			},
			*/
			{
				$project: {
					m_size: {$size: "$m"},
					restaurantName: 1,
					eloScore: 1,
					m: 1
				}
			},
			{
				$match: {
					m_size: {$gte: 1}
				}
			},
			{
				$sort: {
					eloScore: -1
				}
			},
			{$unwind: "$m"}, 
			{$sort: {"m.availableDate":-1}},
			{
				$group:{
					_id: {_id: "$_id", restaurantName: '$restaurantName', eloScore: '$eloScore'},
					// restaurantName: {'$restaurantName'},
					// eloScore: 'a',
					latestItem: {$first: '$m'}
				}
			},
			{
				$project: {
					latestItem: 1,
					restaurantName: '$_id.restaurantName',
					eloScore: '$_id.eloScore',
					_id: '$_id._id'
				}
			},
			{$sort: {'latestItem.availableDate': -1}},
			{
				$limit: 6
			}
			], function(err, results){
				if (err) {res.send(err);}
				else res.send(results);
			});
		}

	};

}



function checkUserRelationshipInfo(self, users, callback){

	async.each(users, function(user, cb){

		var foundUser = _.find(self.friends, function(friend){
			return friend.toString() === user._id.toString();
		});
		
		var pendingRequest = _.find(self.pendingFriendRequests, function(pending){
			return pending.toString() === user._id.toString();
		});

		var followingUser = _.find(self.following, function(following){
			return following.toString() === user._id.toString();
		});

		var friendRequest = _.find(self.friendRequests, function(friendReq){
			return friendReq.toString() === user._id.toString();
		});

		user.isFriend = foundUser ? 1 : 0;
		user.isPendingRequest = pendingRequest ? 1 : 0;
		user.isFollowingUser = followingUser ? 1 : 0;
		user.isWaitingForConfirmation = friendRequest ? 1 : 0;

		cb(null);

	}, function(err){
		callback(err, users);
	
	});

}

dailySales = function(restaurantId, cb){

	var o = {};
	o.map = function(){
		var day = {
			year: this.orderDate.getFullYear(),
			month: this.orderDate.getMonth(),
			day: this.orderDate.getDate()
		}
		var quantityValue = {
			quantity: this.quantity,
			amount: this.orderAmount,
			currency: this.currency
		};

		emit(day, quantityValue);

	}

	o.reduce = reduceFunction;

	var today = new Date();

	var y = today.getFullYear();
	var m = today.getMonth();

	var start = new Date(y, m, 1);

	o.query = {orderDate:{$gte: start, $lt: new Date()}, sellerRestaurant: restaurantId};

	Order.mapReduce(o, function(err, data){
		if (err) {cb(err);}
		else{
			_.each(data, function(sales){

				if (sales.value.amount) {
					sales.value = sales.value.amount;
				}

			});

			cb(null, data);

		}
	});

}


totalSales = function(o, cb){

	Order.mapReduce(o, function(err, data, stats){

		if (err) {console.log('err', err); next(err);}
		else{

			data
			.find()
			.populate({path:'_id', select: 'restaurantName currency', model: 'Restaurant'})
			.exec(function(err, result){
				if (err) {console.log('err', err); cb(err);}
				else{


					async.each(result, function(restaurant, callback){

						if (restaurant._id.currency != 'HKD' || restaurant.value.amount) {

							convertCurrency(restaurant._id.currency, restaurant.value, function(err, amount){
								if (err) {callback(err);}
								else{
									restaurant.value = amount;
									restaurant.restaurantName = restaurant._id.restaurantName;
									restaurant._id = restaurant._id._id;
									callback(null);
								}
							});

						}else{

							if (restaurant.value.amount) {
								restaurant.value = restaurant.value.amount;
							}

							restaurant.restaurantName = restaurant._id.restaurantName;
							restaurant._id = restaurant._id._id;

							callback(null);
						}


					}, function(err){
						result.sort(function(a, b){
							if (a && b) {
								return (a.value < b.value) ? -1 : (a.value > b.value) ? 1 : 0;
							}
						});

						cb(null, result);

					});


				}

			});

		}
	});


}



var convertCurrency = function(currency, amount, callback){

	async.waterfall([
		function(callback){
			ExchangeRate.find({}).lean().sort('-updated').exec(function(err, result){
				callback(err, result[0]);
			});
		},
		function(ex, callback){

			fx.base = ex.base;
			fx.rates = ex.data;
			if (!amount) {return callback(new Error("AMOUNT_ERROR"));}
			if (!currency) {return callback(new Error("CURRENCY_ERROR"));}
			callback(null, fx(amount).from(currency).to('HKD'));
		}	

		], function(err, result){
			callback(err, result);
	});

}

var mapFunction = function(){
	emit(this.sellerRestaurant, this.orderAmount)
}


var reduceFunction = function(key, values){
	var reduced = 0;
	for (var i = 0; i < values.length; i++) {
		reduced += values[i].amount
	};
	return reduced;
}

function calculateScores(restaurants, winnerRestaurant, cb){
	
	if (restaurants.length <= 1) {return cb(null, restaurants);}

	var restaurantsCopy = [];

	for (var i = 0; i < restaurants.length; i++) {

		var a = {};

		if (restaurants[i].id === winnerRestaurant.id) {
			a.place = 1;
		}else{
			a.place = 2;
		}

		a.eloPost = 0;
		a.eloChange = 0;
		a.eloPre = restaurants[i].eloScore;
		a._id = restaurants[i]._id;
		restaurantsCopy.push(a);

	}
	
	process.nextTick(function(){
	
		var n = restaurants.length;
		var change = Array.apply(null, Array(n)).map(Number.prototype.valueOf,0);
		var K = 32 / (n - 1);

		for (var i = 0; i < n; i++) {

			var myElo = restaurantsCopy[i].eloPre;
			var place = restaurantsCopy[i].place;
			for (var j = 0; j < n; j++){
				if (i != j) {

					var opponentPlace = restaurantsCopy[j].place;
					var opponentElo = restaurantsCopy[j].eloPre
					if (place < opponentPlace){
						S = 1;
					}else if(place == opponentPlace){
						S = 0.5;
					}else
						S = 0;

					var EA = 1 / (1.0 + Math.pow(10.0, (opponentElo - myElo) / 400.0));
					restaurantsCopy[i].eloChange += Math.round(K * (S - EA));
				}
			}

			restaurantsCopy[i].eloPost = restaurantsCopy[i].eloPre + restaurantsCopy[i].eloChange;

		}
	
		cb(null, restaurantsCopy);

	});

}


