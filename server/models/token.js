var mongoose = require('mongoose');
var connection = require('../../config/tokenConnection'),
	userConnection = require('../../config/userDBConnection');


var tokenSchema = mongoose.Schema({

	token: {type: String, required: true},
	os: {type: String, required: true},
	user: {type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true},
	lastUsed: {type: Date, default: Date.now}
});

var TokenModel = connection.model('PushNotificationToken', tokenSchema);
module.exports = TokenModel;

