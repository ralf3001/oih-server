var Conversation = require('./model'),
	async = require('async'),
	_ = require('underscore'),
	mongoose = require('mongoose'),
	PushToken = require('../models/token')



module.exports = function () {
	
	return {
		userConversations: function(req, res, next){

			Conversation.find({users: {$in: [req.user.id]}})
						.populate('users', 'name lastName')
						.lean()
						.exec(function(err, conversations){
				if (err) {next(err);}
				else{
					req.conversations = conversations;
					next();
					// res.send(conversations);
				}
			});

		},
		fetchIndividualConversation: function(req, res, next){
			// console.log(req.params)
			Conversation.findOne({$and: [{users: {$in: [req.params.userId]}}, {users: {$in: [req.user.id]}}]})
						.populate('users', 'name lastName')
						.lean()
						.exec(function(err, conversation){
							if (err) {next(err);}
							else{
								req.conversation = conversation;
								next();
							}
						});
		},
		create: function(req, res, next){

			var convo = new Conversation();
			convo.users.push(req.user._id);
			convo.users.push(mongoose.Types.ObjectId(req.body.userId));

			convo.save(function(err, convo){
				if (err) {next(err);}
				else{

					var msg = req.body.message;
					req.body = msg;
					req.conversation = convo;
					
					next();
				
				}
			});

		},
		findById: function(req, res, next, id){
			Conversation.findById(id, function(err, conversation){
				if (err) {next(err);}
				if (!conversation) {return next(new Error("FAILED_TO_FIND_CONVERSATION"))}
				req.conversation = conversation;
				next();
			});
		}
	}
};

