var mongoose = require('mongoose');
var User = require('../models/user');
var connection = require('../../config/crashReportConnection'),
	CrashReport = require('./model');

module.exports = function () {
	
	var pageLimit = 20;

	return {
		create: function(req, res, next){

			var log = new CrashReport(req.body);

			log.crashReason = req.body.crashReason;
			log.callStackSymbols = req.body.callStackSymbols;
			log.version = req.body.systemVersion;
			log.model = req.body.model;
			log.OS = req.body.OS;
			log.timeStamp = req.body.timeStamp;
			log.version =req.body.version;
			log.build = req.body.build;
			log.IPaddress = req.ip;

			log.save(function(err, log){
				if (!err) { res.sendStatus(200);}
				else{
					return next(err);
				} 
			});
		},
		fetch: function(req, res, next){

			var date = req.query.createdDate ? req.query.createdDate : Date.now();
			var limit = req.query.limit ? req.query.limit : pageLimit;

			CrashReprot.find({orderDate: {$lt: date}})
						.lean()
						.limit(limit)
						.exec(function(err, reports){
							if (err) {next(err);}
							else{
								res.send(reports);
							}
						});

		}

	};

}