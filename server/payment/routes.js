module.exports = function (app, auth) {


	var payment = require('./controller')();
	var restaurant = require('../restaurant/controller')();
	var user = require('../user/controller')();

	app.get('/client_token', payment.clientToken);

	app.get('/monthlysales', auth.isLoggedIn, restaurant.fetchUserRestaurants, 
												payment.monthlySales,
												user.findBraintreeUser,
												payment.userPayout);


	// app.route('/payment')
		// .post(auth.isLoggedIn, payment.create)
		// .get(auth.isLoggedIn, payment.all);

	// app.route('/restaurant/:restaurantId')
	// 	.put(auth.isLoggedIn, payment.update)
	// 	.delete(auth.isLoggedIn, payment.destroy)


	// app.param('restaurantId', payment.restaurant);
// 
}