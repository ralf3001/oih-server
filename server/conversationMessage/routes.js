module.exports = function (app, auth) {


	var message = require('./controller')();
	var conversationBlock = require('../conversation/controller')();

	app.route('/message/:conversationId')
		.post(auth.isLoggedIn, message.create, message.notifications)
		.get(auth.isLoggedIn, message.fetch);

	app.param('conversationId', conversationBlock.findById);

}