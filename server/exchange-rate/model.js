var mongoose = require('mongoose');
var User = require('../models/user');
var connection = require('../../config/userDBConnection');

var exchangeRateSchema = mongoose.Schema({

	data: mongoose.Schema.Types.Mixed,
	updated: { type: Date, default: Date.now },
	base: String
});

var exchangeRateModel = connection.model('exchange-rate', exchangeRateSchema);
module.exports = exchangeRateModel;

