module.exports = function (app, auth) {

	var competition = require('./controller')();


	app.route('/competition')
		.post(auth.isLoggedIn, competition.create)

	app.route('/competition/dismiss/:competitionId')
		.post(auth.isLoggedIn, competition.userDismissed)
	

	app.route('/fetchUnvotedCompetition')
		.get(auth.isLoggedIn, competition.fetchUnvotedCompetition);
		
	app.route('/fetchUnjoinedCompetition')
		.get(auth.isLoggedIn, competition.fetchUnjoinedCompetition);


	// app.param('competitionId', competition.competition);

}