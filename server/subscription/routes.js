module.exports = function (app, auth) {


	var subscription = require('./controller')();

	app.route('/subscription')
		.post(auth.isLoggedIn, subscription.create)

	app.route('/subscription/:subscriptionId')
		.post(auth.isLoggedIn, subscription.subscribe)
		.delete(auth.isLoggedIn, subscription.unsubscribe)

	app.route('/subscription/user/:userId')
		.get(auth.isLoggedIn, subscription.user)

	

	app.param('subscriptionId', subscription.subscription);

}