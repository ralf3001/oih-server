var models = require('../app/models');
var User = models.User;

module.exports.requiresUser = function(req, res, next) {

  if (req.session.userId) {
    req.user = { id: req.session.userId }
    next();
  } else {
    debugger;
    res.app.oauth.authorise()(req, res, next);
  }
}

module.exports.loadUser = function(req, res, next) {
  User.findOne({ email: req.session.userId}, function(err, user) {
    if (err) return next(err);
    res.locals.user = user;a
    next();
  });
}

module.exports.isValidationError = function(err) {
  return err && err.name === 'ValidationError';
}

module.exports.notFoundHandler = function(req, res, next) {
  res.status(404);
  res.format({
    html: function() {
      res.render('404', { url: req.url });
    },
    json: function() {
      res.send({ error: 'Not Found' });
    }
  });
}

