var Restaurant = require('../server/restaurant/model'),
	Meal = require('../server/meal/model'),
	async = require('async'),
	_ = require('underscore'),
	oxr = require("open-exchange-rates"),
	fx = require("money");
	
oxr.set({app_id: 'a040d478fe6246b28d748fd6654f5ba2'});



var meals = [

	{
		restaurant : "567133f234079f9a66dba6e3",
		type: "dinner",
		name: "肉絲炒麵",
		priceInLocalCurrency: 45,
		availableDate: new Date("December 8, 2015")
	},
	{
		restaurant : "567133f234079f9a66dba6e4",
		type: "dinner",
		name: "肉絲炒麵",
		priceInLocalCurrency: 45,
		availableDate: new Date("December 8, 2015")
	},
	{
		restaurant : "567133db47571f8f664cf586",
		type: "snack",
		name: "豉油皇炒麵",
		priceInLocalCurrency: 39,
		availableDate: new Date("December 8, 2015")
	},
	{
		restaurant : "567133db47571f8f664cf587",
		type: "snack",
		name: "福建炒飯",
		priceInLocalCurrency: 42,
		availableDate: new Date("December 8, 2015")
	},
	{
		restaurant : "567133db47571f8f664cf58b",
		type: "snack",
		name: "大鑊鳥炒飯",
		priceInLocalCurrency: 37,
		availableDate: new Date("December 8, 2015")
	},
	{
		restaurant : "567133db47571f8f664cf58c",
		type: "dinner",
		name: "咖喱牛肉飯",
		priceInLocalCurrency: 45,
		availableDate: new Date("December 8, 2015")
	},
	{
		restaurant : "567133db47571f8f664cf588",
		type: "dinner",
		name: "Pepperoni Pizza",
		priceInLocalCurrency: 80,
		availableDate: new Date("December 8, 2015")
	},
	{
		restaurant : "567133db47571f8f664cf58f",
		type: "lunch",
		name: "四寶飯",
		priceInLocalCurrency: 35,
		availableDate: new Date("December 8, 2015")
	},
	{
		restaurant : "567133db47571f8f664cf58a",
		type: "snack",
		name: "Chicken Sandwich",
		priceInLocalCurrency: 20,
		availableDate: new Date("December 8, 2015")

	}
];



async.each(meals, function(meal, callback){

	async.waterfall([
		function(callback){
			Restaurant.findById(meal.restaurant, function(err, rest){
				callback(err, rest);
			});
		},
		function(restaurant, callback){

			var m = new Meal(meal);
			m.restaurant = restaurant;
			m.currencyCode = restaurant.currency;
			m.lastUpdate = Date.now();
			oxr.latest(function() {

				fx.rates = oxr.rates;
				fx.base = oxr.base;
				
				// money.js is ready to use:
				console.log(fx(100).from('HKD').to('GBP')); // ~8.0424

			});
			m.save(function(err){
				callback(err, restaurant, m);
			});

		},
		function(restaurant, meal, callback){

			restaurant.meal.push(meal);
			restaurant.save(function(err){
				callback(err);
			})

		}

	], function(err, result){
		if (err) {console.log(err);}
		else{
			console.log("done");
		}
	});


}, function(err){
	if (err) {
		console.log(err);
	}else{
		return;
	}
});

