var bcrypt   = require('bcrypt');
var userConnection = require('../../config/userDBConnection')
var mongoose = require('mongoose');

var keySchema = mongoose.Schema({
	publicKey: String,
	privateKey: String,
	os: String,
});

var keyModel = userConnection.model('rsakeys', keySchema);
module.exports = keyModel;
