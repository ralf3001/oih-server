module.exports = function (app, auth) {

	var entry = require('./controller')();
	var competition = require('../competition/controller')();

	app.route('/competitionEntry/:competitionId')
		.post(auth.isLoggedIn, entry.create)
		.get(auth.isLoggedIn, entry.fetch)


	app.route('/competitionEntry/vote')
		.post(auth.isLoggedIn, entry.vote)

	app.route('/competitionEntry/frontRunners/:competitionId')
		.get(auth.isLoggedIn, entry.frontRunners)
		
	// app.route('/meal/:mealId')
	// 	.delete(auth.isLoggedIn, meals.destroy)

	app.param('competitionId', competition.competition);

}