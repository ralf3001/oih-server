var mongoose = require('mongoose');
var connection = require('../../config/tokenConnection');


var tokenSchema = mongoose.Schema({
	token: String,
	expiryDate: Date
});

var TokenModel = connection.model('Token', tokenSchema);
module.exports = TokenModel;

