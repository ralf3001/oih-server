var Message = require('./model'),
	async = require('async'),
	_ = require('underscore'),
	mongoose = require('mongoose'),
	Conversation = require('../conversation/model'),
	PushToken = require('../models/token'),
	apn = require('apn')


module.exports = function () {

	var pageLimit = 15;

	return {
		create: function(req, res, next){
			
			var message = new Message();
			
			if (req.conversation) {
				message.conversation = req.conversation.id;
			}else{
				message.conversation = req.body._id;
			}

			message.sender = req.user.id;
			message.content = req.body.content;
			message.url = req.body.url;
			message.type = req.body.type;

			message.save(function(err){
				next(err);
			});

		},
		fetch: function(req, res, next){
			
			var createDate = req.query.createDate ? new Date(req.query.createDate) : Date.now();
			var conversation = req.conversation;
			var query = {};
			var sort = '';

			if (conversation) {
				
				if (req.query.forward == true) {
					query = {conversation: conversation._id, createDate: {$gt: createDate}}
					sort = 'createDate';
				}else{
					query = {conversation: conversation._id, createDate: {$lt: createDate}};
					sort = '-createDate';
				}

				Message.find(query)
						.sort(sort)
						.limit(pageLimit)
						.lean()
						.exec(function(err, messages){
							if (err) {next(err);}
							else{
								if (req.query.forward == true) {}
								else{
									messages = _.sortBy(messages, function(message){return message.createDate});
								}
								res.send(messages);
							}
						});

			}else{

				var convo = new Conversation();
				convo.users.push(req.user._id);
				convo.users.push(mongoose.Types.ObjectId(req.params.userId));

				convo.save(function(err, convo){
					if (err) {next(err);}
					else{
						res.send(convo);					
					}
				});

			}

		},
		notifications: function(req, res, next){

			var users = req.conversation.users;
			var ids = _.filter(users, function(user){return user.toString() !== req.user.id.toString()});

			fetchPushTokens(ids, 'ios', function(err, tokens){
				if (err) {callback(err);}

				process.nextTick(function(){
					async.each(tokens, function(tokenDocument, cb){

						var device = new apn.Device(tokenDocument.token)
						var apnConnection = new apn.Connection(apnOptions);

						var note = new apn.Notification();

						note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
						note.sound = "ping.aiff";
						note.alert = req.user.name + ' has sent you a message';
						note.payload = {"message": "com.ralfcheung.OIH.MessageUserWillFetchMessage", "id": req.conversation._id};
						apnConnection.pushNotification(note, device);
						cb(null);

					}, function(err){
						if (err) {next(err);}
						else{
							res.sendStatus(200);
						}
					});
				});

			});
		},
		lastMessage: function(req, res, next){

			async.each(req.conversations, function(conversation, callback){
				
				Message.find({conversation: conversation._id, createDate: {$lt: Date.now()}})
						.sort('-createDate')
						.limit(1)
						.lean()
						.exec(function(err, message){
							if (err) {callback(err);}
							else{
								conversation.lastMessage = message[0];
								callback(null);
							}
			
						});

			}, function(err){
				if (err) {callback(err);}		
				res.send(req.conversations);
			});

		}
	}

}


fetchPushTokens = function(users, os, callback){

	process.nextTick(function(){

		PushToken.find({"user": {$in: users}, "os": os}).exec(function(err, tokens){
			callback(err, tokens);
		});

	});

}