var mongoose = require('mongoose'),
	connection = require('../../config/userDBConnection'),
	user = require('../models/user')



var addressSchema = mongoose.Schema({

	tag: {type: String, default: 'default'},
	geometry: {
		type: {type: String},
		coordinates: []
	   // coordinates: { coordin: [Number], index: '2dsphere'}
	},
	userEnteredAddress: String,
	formattedAddress: String,
	
	street: String,
	city: String,
	state: String,
	postalCode: String,
	ISOCountryCode: String,

	addressComponents: [],
	user: {type: mongoose.Schema.Types.ObjectId, ref: 'User'}
});

addressSchema.index({geometry: '2dsphere'});

var addressModel = connection.model('Address', addressSchema);
module.exports = addressModel