module.exports = function (app, auth) {


	var couponroutes = require('./controller')();

	app.route('/applycode')
		.post(auth.isLoggedIn, couponroutes);

}