var mongoose = require('mongoose');
var User = require('../models/user');
var connection = require('../../config/userDBConnection');

var restaurantSchema = mongoose.Schema({

	restaurantName: String,
	address: {
		city: String,
		streetAddress: String,
		postalCode: String,
		region: String,
		locality: String,
		country: {type: String, required: true}

	},
	geometry: {
		type: {type: String},
		coordinates: []
	},

	addressString: String,
	userEnteredAddress: String,

	phoneNumber: String,
	braintreeMerchantAccountId: String,
	eloScore: {type: Number, default: 1000},
	currency: {type: String, default: "HKD"},
  	accountsAssociatedTo: [{type: mongoose.Schema.Types.ObjectId, ref: 'User'}],
  	administrators: [{type: mongoose.Schema.Types.ObjectId, ref: 'User'}],
	meal: [{type: mongoose.Schema.Types.ObjectId, ref: 'Meal'}],
	createdAt: {type: Date, default: Date.now()},
	operatingHours:{
		type: 
			[
				{
					opening: Date, 
					closing: Date, 
					dayOfWeek: {
						type: String,
						enum: ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su']
					}
				}
			],
		validate: [arrayLimit, '{PATH} exceeds limit of 7']
	},
	isDestroyed: {type: Boolean, default: false},
	sellersNote: String,
	restaurantPictures: [String]

});


function arrayLimit(val){
	return val.length <= 7;
}

restaurantSchema.index({geometry: '2dsphere'});



// mongooseRedisCache(mongoose)

// mongooseRedisCache(mongoose, {
//    host: "redisHost",
//    port: "redisPort",
//    pass: "redisPass",
//    options: "redisOptions"
//  });




var RestaurantModel = connection.model('Restaurant', restaurantSchema);
module.exports = RestaurantModel;

