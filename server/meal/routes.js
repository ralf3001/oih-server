module.exports = function (app, auth) {
	// body...

	var meals = require('./controller')();
	var activity = require('../activity/controller')();

	app.route('/meal')
		.post(auth.isLoggedIn, meals.create, activity.create)
		.get(auth.isLoggedIn, meals.fetch);
		
	app.route('/meal/:mealId')
		.delete(auth.isLoggedIn, meals.destroy)

	app.param('mealId', meals.meal);

}