var Activity = require('./model'),
	async = require('async'),
	Order = require('../order/model'),
	Meal = require('../meal/model'),
	Restaurant = require('../restaurant/model'),
	mongoose = require('mongoose'),
	_ = require('underscore')


module.exports = function(){
	var pageLimit = 10;

	return{

		create: function(req, res, next){

			var activity = new Activity();
			activity.fromUser = req.user.id;

			switch(req.body.activityType){

				case "orderFood":{
					activity.activityType = "orderFood";
					activity.order = req.order;
					
					if (req.order.recipientUser) {
						activity.toUser = req.order.recipientUser
					}

					break;
				}
				case "like":{
					activity.activityType = "like";
					Activity.findById(req.body.activityId, function(err, activityToLike){
						if (err) {next(err)}
						else{
							activity.activity = activityToLike.id;
							activity.save(function(err, act){
								if (err) {next(err);}
								else{
									res.sendStatus(200);
								}
							});

						}
					});
					break;
				}
				case "thank":{

					activity.activityType = "thank";
					Activity.findById(req.body.activityId, function(err, activityToThank){
						if (err) {next(err);}
						else{
							activity.activity = activityToThank.id;
							activity.save(function(err, act){
								if (err) {next(err);}
								else{
									res.sendStatus(200);
								}
							});

						}
					});

					break;
				}
				case "newItem":{
					activity.activityType = "newItem";
					activity.meal = req.meal.id;
					activity.postPicture = req.meal.productImage;
					activity.save(function(err, act){
						if (err) {next(err);}
						else{
							res.sendStatus(200);
						}
					});

					break;
				}
				case "comment":{
					activity.activityType = "comment";
					activity.activity = req.body.activityId;
					activity.content = req.body.content;

					activity.save(function(err, act){
						if (err) {next(err);}
						else{
							res.sendStatus(200);
						}
					});
					
					break;
				}
				default:

					break;
			}

		},
		orderFoodActivity: function(req, res, next){
			
			var activity = new Activity();
			activity.fromUser = req.user.id;
			activity.activityType = "orderFood";

			activity.order = req.order.id;
			if (req.order.recipientUser) {
				activity.toUser = req.order.recipientUser;
			}
			
			activity.save(function(err){
				next(err);
			});
		},
		fetchUserActivities: function(req, res, next){

			var frds = req.user.friends;
			var feedDate = req.query.feedDate ? new Date(req.query.feedDate) : Date.now();

			async.waterfall([

				function(callback){
					//only fetch orders and 
					Activity.find({$and:[{$or:[{activityType: 'orderFood'}, {activityType: 'review'}, {activityType: 'newItem'}]}, {$or:[{fromUser: {$in: frds}}, {toUser: {$in: frds}}]}], createdAt: {$lt: feedDate}})
					.select('postPicture postScore restaurant fromUser toUser order activity orderMeal meal activityType content createdAt rating audioNote')
					.populate('fromUser toUser order activity orderMeal meal', 'restaurant name audioReview orderDate orderMeal sellerRestaurant quantity comment name profilePic')
					.lean()
					.sort('-createdAt').limit(pageLimit).exec(function(err, activities){
						callback(err, activities);
					});
				},

				function(activities, callback){

					populateActivies(activities, function(err, data){
						callback(err, data);
					});

				}, 
				function(activities, callback){
					
					async.each(activities, function(act, cb){
						Activity.find({activity: act, activityType: 'thank', fromUser: req.user}).lean().exec(function(err, foundAct){
							if (err) {cb(err);}
							else{
								act.userAlreadyThank = foundAct.length > 0;
							}
							cb();
						});
					}, function(err){
						callback(null, activities);
					});

				}

				], function(err, result){

					if (err) {next(err);}
					else{res.send(result);}

			});

		},
		deleteSubActivity: function(req, res, next){

			var type = req.query.activityType;
			var activityId = req.query.activityId;
			Activity.remove({activity: (activityId), activityType: type, fromUser: req.user.id}, function(err, activity){
				if (err) {next(err);}
				else {res.sendStatus(200);}
			});

		},
		deleteParentActivity: function(req, res, next){

			var activityId = req.query.activityId;

			Activity.findByIdAndRemove(activityId).exec(function(err, act){
				if (err) {next(err);}
				else{
					Activity.remove({activity: act}).exec(function(err, activities){
						if (err) {next(err);}
						else {res.sendStatus(200);}
					});
				}
			});

		},
		fetchAttributedActivity: function(req, res, next){

			Activity.find({activity: req.query.activityId}).exec(function(err, act){
				if (err) {next(err);}
				else{
					res.send(act);
				}
			});

		},
		ordercomment: function(req, res, next){

			var activity = new Activity();

			activity.content = req.body.comment;		
			activity.fromUser = req.user;
			activity.order = mongoose.Types.ObjectId(req.body.orderId);
			activity.activityType = "review";
			activity.rating = req.body.rating;

			if (req.body.image_url) {

				activity.postPicture = req.body.image_url;
				activity.save(function(err, act){
					if (err) {next(err);}
					else{
						res.send(act);
					}
				});
			
			}else{
			
				Order.findById(req.body.orderId).populate('orderMeal').select('orderStatus orderMeal productImage').lean().exec(function(err, order){
					if (order) {
						activity.postPicture = order.orderMeal.productImage;
						activity.save(function(err, act){
							if (err) {next(err);}
							else{

								var a = act.toObject();

								delete order.orderMeal;
								a.order = order;
								delete a.sellerRestaurant;
								delete a.fromUser;

								res.send(a);

							}
						});

					}

				});

			}

		},
		fetch: function(req, res, next){

			fetchOrderComment(req.query, function(err, activities){
				if (err) {next(err);}
				else{
					res.send(activities);
				}
			});
		},
		update: function(req, res, next){

			var id = req.body._id;
			var body = req.body;

			delete body._id;

			Activity.findByIdAndUpdate(id, {$set: body}).exec(function(err, act){
				if (err) {return next(err);}
				else{
					res.sendStatus(200);
				}
			});

		},
		postcomment: function(req, res, next){

			var feedDate = req.query.feedDate ? req.query.feedDate : Date.now();

			Activity.find({activity: req.query.activityId, activityType: 'comment', createdAt: {$lt: feedDate}})
					.select('content fromUser createdAt')
					.populate('fromUser', 'name lastName profilePic')
					.limit(20)
					.lean()
					.sort('createdAt')
					.exec(function(err, comments){
				if (err) {next(err);}
				else{
					res.send(comments);
				}
			});

		},
		fetchRestaurantFeed: function(req, res, next){

			var feedDate = req.query.feedDate ? req.query.feedDate : Date.now();

			async.waterfall([

				function(callback){
					Order.find({sellerRestaurant: req.query.restaurant, orderDate: {$lt: feedDate}})
					.limit(8)
					.lean()
					.sort('-orderDate')
					.exec(function(err, orders){
						callback(err, orders);
					});
				},
				function(orders, callback){
					
					var orderIds = _.pluck(orders, '_id');
					Activity.find({$and:[
											{$or:[ 
												{activityType: 'orderFood'}, 
												{activityType: 'review'}, 
												{activityType: 'newItem'}
												]
											}
										],
										order: {$in: orderIds} 
									})
					.select('postPicture postScore restaurant fromUser toUser order activity orderMeal meal activityType content createdAt rating audioNote')
					.populate('fromUser toUser order activity orderMeal meal', 'restaurant name audioReview orderDate orderMeal sellerRestaurant quantity comment name profilePic')
					.lean()
					.sort('-createdAt').limit(8).exec(function(err, activities){
						callback(err, activities);
					});

				},
				function(activities, callback){

					populateActivies(activities, function(err, data){
						callback(err, data);
					});

				}



				], function(err, result){
					if (err) {next(err);}
					else{res.send(result);}

			});

		}
	}

}


function populateActivies (activities, callback) {
	 
	var d = new Date();

	async.waterfall([

		function(cb){
			process.nextTick(function(){
				Meal.populate(activities, {
					path: 'meal.restaurant',
					select: 'restaurantName',
					model: 'Restaurant'
				}, function(err, m){
					// console.log('2', d.getTime());
					cb(err, m);
				});
			})
		},
		function(m, cb){
			process.nextTick(function(){

				Order.populate(m, {
					path: 'order.sellerRestaurant',
					select: 'restaurantName quantity ',
					model: 'Restaurant'
				}, function(err, orders){
					// console.log('3', d.getTime());
					cb(err, orders);
				});
			});
		},
		function(o, cb){

			Meal.populate(o,{
				path: 'order.orderMeal',
				select: 'name -_id productImage'
			}, function(err, meals){
				cb(err, meals);
			});

		}
	], function(err, activities){
		callback(err, activities);
	});

}

function fetchOrderComment (query, callback) {

	Activity.findOne(query)
	.populate('order')
	.select('order.id postPicture activityType rating')
	.lean().exec(function(err, activities){
		callback(err, activities);
	});

}

