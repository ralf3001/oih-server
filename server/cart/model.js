var mongoose = require('mongoose'),
	connection = require('../../config/userDBConnection')

var cartSchema = mongoose.Schema({

	items: [{type:mongoose.Schema.Types.ObjectId, ref: 'Meal'}],
	lastUpdated: {type: Date, default: Date.now},
	user: {type: mongoose.Schema.Types.ObjectId, ref:'User'}
});


var cartModel = connection.model('Cart', cartSchema);
module.exports = cartModel;
