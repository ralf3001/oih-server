var mongoose = require('mongoose')
var configDB = require('./database.js');


var options = {
  db: { native_parser: true },
  server: { poolSize: 5 },
  user: process.env.userDBUser,
  pass: process.env.userDBPass
}

module.exports = connectinoOne = mongoose.createConnection(configDB.tokenDB, options);

connectinoOne.on('connected', function(){
    console.log('Connected to Push Notification Tokens DB');
})

require('../server/models/token');