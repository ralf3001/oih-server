module.exports = function (app, auth){

	var order = require('./controller')();
	var receipt = require('../receipt/controller')();
	var meal = require('../meal/controller')();
	var restaurant = require('../restaurant/controller')();
	var activity = require('../activity/controller')();
	var search = require('../searchResults/controller')();


	app.route('/order/:orderId')
		.get(auth.isLoggedIn, order.orderDetail);

	app.route('/order')
		.get(auth.isLoggedIn, order.user)
		.post(auth.isLoggedIn, 
				receipt.create, 
				restaurant.findById,
				meal.findById, 
				order.create, 
				activity.orderFoodActivity,
				search.update,
				restaurant.updateScores
				);

	app.route('/mostRecentOrder')
		.get(auth.isLoggedIn, order.mostRecentOrder);

	app.route('/orderread')
		.get(auth.isLoggedIn, order.acknowledgeOrders);

	app.route('/unackordersCount')
		.get(auth.isLoggedIn, order.unAcknowledgedOrders);

	app.route('/orderaudionote')
		.post(auth.isLoggedIn, order.audioNote);

	// app.route('/transaction')
	// 	.get(auth.isLoggedIn, order.findTransaction);

		
//for restaurant owners only
	app.route('/restaurantOrders')
		.get(auth.isLoggedIn, order.restaurantOrders);
	
	app.route('/orderStatus')
		.post(auth.isLoggedIn, order.orderStatus);

	// app.param('username', order.user);
	app.param('orderId', order.order);

}