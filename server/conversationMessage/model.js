var mongoose = require('mongoose'),
	connection = require('../../config/userDBConnection')

var messageSchema = mongoose.Schema({

	sender: {type:mongoose.Schema.Types.ObjectId, ref: 'User'},
	conversation: {type: mongoose.Schema.Types.ObjectId, ref: 'ConversationBlock'},
	createDate: {type: Date, default: Date.now},
	content: {type: String},
	type: {type: String, enum: ['string', 'picture', 'audio']},
	url: {type: String}
	
});


var messageModel = connection.model('ConversationMessage', messageSchema);
module.exports = messageModel