var mongoose = require('mongoose');
var User = require('../models/user');
var connection = require('../../config/userDBConnection');

var subscriptionSchema = mongoose.Schema({

	subscriptionName: String,
	subscriptionDescription: String,

  	user: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
  	priceInUSD: Number,
	priceInLocalCurrency: Number,
	priceInLocalCurrencyString: String,
	currencyCode: String,
	createdAt: {type: Date, default: Date.now},
  	subscribers: [{type: mongoose.Schema.Types.ObjectId, ref: 'User'}]


});


var SubscriptionModel = connection.model('subscription', subscriptionSchema);
module.exports = SubscriptionModel;

