var mongoose = require('mongoose')
var configDB = require('./database.js');


var options = {
  db: { native_parser: true },
  server: { poolSize: 5 },
  user: process.env.crashLogDBUser,
  pass: process.env.crashLogDBPass
}

module.exports = connectinoOne = mongoose.createConnection(configDB.crashReport, options);

connectinoOne.on('connected', function(){
    console.log('Connected to Crash Report DB');
})

require('../server/crashReport/model');