module.exports = function (app, auth) {


	var restaurants = require('./controller')();

	app.route('/restaurant')
		.post(auth.isLoggedIn, restaurants.create)
		.get(auth.isLoggedIn, restaurants.all);

	app.route('/restaurant/:restaurantId')
		.get(restaurants.user, restaurants.information)
		.put(auth.isLoggedIn, restaurants.update)
		.delete(auth.isLoggedIn, restaurants.userAdministerRestaurant, restaurants.destroy)

	app.route('/calculateReceipt')
		.get(restaurants.orderAmountToReceipt);

	app.route('/statistics')
		.get(restaurants.fetchUserRestaurants, restaurants.statistics);
	
	app.route('/restaurant/rating')
		.post(auth.isLoggedIn, restaurants.restaurantRating);

	app.route('/latestitems')
		.get(auth.isLoggedIn, restaurants.recentlyAdded);	
	app.param('restaurantId', restaurants.restaurant);

}