var User = require('../models/user'),
_ = require('underscore'),
geocoder = require('geocoder'),
async = require('async'),
Token = require('../models/token'),
Address = require('../address/model'),
crypto = require('crypto'),
algorithm = 'aes-256-ctr',
NodeRSA = require('node-rsa'),
RSAKey = require('../models/rsa-key'),
User = require('../models/user'),
FB = require('fb'),
braintree = require("braintree"),
Meal = require('../meal/model'),
mongoose = require('mongoose'),
Mailgun = require('mailgun-js');

gateway = braintree.connect({
	environment: braintree.Environment.Sandbox,
	merchantId: process.env.braintreeMerchantId,
	publicKey: process.env.braintreePublicKey,
	privateKey: process.env.braintreePrivateKey
});



module.exports = function() {

	return {

		findUserByEmail: function(req, res, next){

			var page = req.query.page;
			var username = req.query.username;
			var regex = {$regex: username, $options: 'i'};

			User.find({ $or: [{'local.email': regex}, {'name': regex}], "_id":{$ne: req.user._id}})
			.select('_id local.email name allowToBeAddedAsFriend isMerchant')
			.lean()
			.exec(function(err, users){

				if (err) {next(err);}
				else if(users){

					async.each(users, function(user, cb){

						var foundUser = _.find(req.user.friends, function(friend){
							return friend.toString() === user._id.toString();
						});
						
						var pendingRequest = _.find(req.user.pendingFriendRequests, function(pending){
							return pending.toString() === user._id.toString();
						});

						var followingUser = _.find(req.user.following, function(following){
							return following.toString() === user._id.toString();
						});

						var friendRequest = _.find(req.user.friendRequests, function(friendReq){
							return friendReq.toString() === user._id.toString();
						});

						user.isFriend = foundUser ? 1 : 0;
						user.isPendingRequest = pendingRequest ? 1 : 0;
						user.isFollowingUser = followingUser ? 1 : 0;
						user.isWaitingForConfirmation = friendRequest ? 1 : 0;

						cb(null);

					}, function(err){
						res.send(users);
					});

				}else res.send(users);
			});
			
		},
		addNotificationToken: function(req, res, next){

			var deviceToken = req.body.deviceToken;
			var deviceType = req.body.device;
			var userId = req.body.userId;

			if (deviceToken && deviceType && userId) {

				User.findByIdAndUpdate(userId).exec(function(err, user){
					if (err) {next(err);}
					if(!user){ next(new Error("no such user"));}


					var newToken = new Token();
					newToken.token = deviceToken;
					newToken.os = deviceType;
					newToken.user = user.id;
					
					var upsertData = newToken.toObject();
					delete upsertData._id;

					Token.update({token: newToken.token}, upsertData, {upsert: true}, function(err, token){
						next(err);
					});


				});

			}else{
				next();
			}
		},
		deleteNotificationToken: function(req, res, next){

			var deviceToken = req.body.deviceToken;
			var deviceType = req.body.device;
			var userId = req.body.userId;

			if (deviceToken && deviceType && userId) {
				User.findByIdAndUpdate(userId).exec(function(err, user){
					if (err) {next(err);}
					if (!user) {next(new Error("no such user"))}

						Token.remove({token: deviceToken, "os": deviceType}).exec(function(err){
							next(err);
						});

				});

			}
		},
		socialTokens: function(req, res, next){

			var type = req.body.type;

			if(type === 'facebook'){
				
				var userId = req.body.userId;
				var accessToken = req.body.access_token;
				var expiry_date = req.body.expiry_date;
				
				// var token = _.filter(req.user.socialTokens, function(token){
				// 	return token.access_token = accessToken;
				// });

				var fb = {};
				fb.userId = userId;
				fb.access_token = accessToken;
				fb.expiry_date = expiry_date;
				fb.socialNetwork = type;

				delete fb._id;
				
				// req.user.socialTokens.push(fb);
				User.findByIdAndUpdate(req.user._id, {$addToSet: {"socialTokens": fb}}, function(err, user){
					if (err) {next(err);}
					else{
						res.sendStatus(200);
					}

				});

			}else{
				res.sendStatus(200);

			}
		},
		removeSocialtoken: function(req, res, next){
			User.findByIdAndUpdate(req.user._id, {$set: {'socialTokens':[]}}, function(err){
				// next(err);
				if (err) {next(err);}
				else{
					res.sendStatus(200);
				}
			});
		},
		addprofilepic: function(req, res, next){
			var url = req.body.url;
			req.user.profilePic.unshift(url);
			req.user.save(function(err){
				if (err) {next(err);}
				else{
					res.sendStatus(200);
				} 
			});
		},
		updateProfile: function(req, res, next){			
			// req.body.field;
			var field = Object.keys(req.body)[0];
			
			if (Array.isArray(req.user[field])) {
				if (field === 'phoneNumber') {
					var phone = {};
					phone.number = req.body[field];
					req.user[field].unshift(phone);
				}else{
					req.user[field].unshift(req.body.field);					
				}
			}else{
				req.user[field] = req.body.field;
			}

			req.user.save(function(err, u){
				if (err) {next(err);}
				else res.sendStatus(200);
			});

		},
		fbfriends: function(req, res, next){

			// var token = req.user.socialTokens[req.user.socialTokens.length - 1].access_token;
			var token = req.query.access_token;
			FB.setAccessToken(token);

			FB.api('me', 'get', {"fields": "id, name, first_name, last_name, picture.type(large), email, birthday, friends ,hometown , friendlists"}, function(result){
				if (!result || result.error) {
					console.log(result.error);
				}else{

					if (result.friends.data.length > 0) {

						var ids = _.pluck(result.friends.data, 'id');

						User.find({'socialTokens':{ $elemMatch: {"userId": {$in:ids},  "socialNetwork": "facebook"} }})
						.lean()
						.select('_id local.email name allowToBeAddedAsFriend isMerchant')
						.exec(function(err, friends){

							if (err) {next(err);}
							else if(friends){

								async.each(friends, function(user, cb){

									var foundUser = _.find(req.user.friends, function(friend){
										return friend.toString() === user._id.toString();
									});
									
									var pendingRequest = _.find(req.user.pendingFriendRequests, function(pending){
										return pending.toString() === user._id.toString();
									});

									var followingUser = _.find(req.user.following, function(following){
										return following.toString() === user._id.toString();
									});

									var friendRequest = _.find(req.user.friendRequests, function(friendReq){
										return friendReq.toString() === user._id.toString();
									});

									user.isFriend = foundUser ? 1 : 0;
									user.isPendingRequest = pendingRequest ? 1 : 0;
									user.isFollowingUser = followingUser ? 1 : 0;
									user.isWaitingForConfirmation = friendRequest ? 1 : 0;

									cb(null);

								}, function(err){
									console.log(friends)
									res.send(friends);
								});

							}else res.send(friends);
						});
					}else{
						res.send(result.friends.data);
					}
				}
			})
		},
		forgotpw: function(req, res, next){
			async.waterfall([
				function(cb){
					crypto.randomBytes(20, function(err, buf) {
						var token = buf.toString('hex');
						cb(err, token);
					});
				},
				function(token, cb){

					User.findOne({'local.email': req.body.email}, function(err, user) {

						if (!user) {
							cb(new Error("FORGOT_PASSWORD_EMAIL_NO_SUCH_USER"));
						}else{

							user.resetPasswordToken = token;
							user.resetPasswordExpires = Date.now() + 3600000; // 1 hour

							user.save(function(err) {
								cb(err, token, user);
							});

						}

					});
				},
				function(token, user, cb){

					var link = ('http://' + req.headers.host + '/reset?token=' + token);
					res.render('resetPasswordEmail', {
														resetLink: link, 
														user: user, 
														mailtoAddress: 'support@getshanon.com', 
														mailtoName: 'Shanon Support Team'
													}, function(err, text){
						cb(err, text, user);
					});
				}


				], function(err, html, user){
					if (err) {
						res.send(500, {"message": err.message});
					}else{

						sendSupportEmail(user, html, 'Shanon Password Reset', function(err){
							if (err) {next(err);}
							else{
								res.sendStatus(200);						
							}
						});
					}
					
				});
		},
		//Present the password reset web page
		checkresetPassword: function(req, res){

			User.findOne({ resetPasswordToken: req.query.token, resetPasswordExpires: { $gt: Date.now() } })
			.select('_id local.email name lastName')
			.exec(function(err, user) {

				if (!user) {
					req.flash('error', 'Password reset token is invalid or has expired.');
					return res.redirect('/forgot');
				}else{
					// req.flash('error', 'Password reset token is invalid or has expired.');

					res.render('resetPasswordWeb', {
						user: user,
						mailtoAddress: 'support@getshanon.com',
						mailtoName: 'Shanon Support Team'
					});
				}


			});
		},
		resetPassword: function(req, res){
			async.waterfall([

				function(cb){
					User.findOne({ resetPasswordToken: req.query.token, resetPasswordExpires: { $gt: Date.now() } }, function(err, user) {
						if (!user) {
							req.flash('error', 'Password reset token is invalid or has expired.');

						}else{

							user.local.password = user.generateHash(req.body.password);
							user.resetPasswordToken = undefined;
							user.resetPasswordExpires = undefined;

							user.save(function(err) {
								cb(err, user);					
							});

						}
					});

				}, function(user, cb){

					res.render('resetPasswordConfirmationEmail', {
									user: user, 
									mailtoAddress: 'support@getshanon.com', 
									mailtoName: 'Shanon Support Team'
								}, function(err, text){
						cb(err, text, user);
					});

				}, function(html, user, cb){
					sendSupportEmail(user, html, 'Shanon Password Changed', function(err){
						cb(err);
					});

				}

				], function(err, html, user){
					if (err) {
						res.send(500, {"message": err.message});
					}else{
						res.render('resetPasswordConfirmationWeb', {user: user});
					}

				});
		},
		fetchUser: function(req, res, next){

			User.findById(req.params.id)
			.select('_id local.email name allowToBeAddedAsFriend isMerchant')
			.lean().exec(function(err, user){
				if (err) {next(new Error("NO_SUCH_USER"));}
				else{

					var foundUser = _.find(req.user.friends, function(friend){
						return friend.toString() === user._id.toString();
					});
					
					var pendingRequest = _.find(req.user.pendingFriendRequests, function(pending){
						return pending.toString() === user._id.toString();
					});

					var followingUser = _.find(req.user.following, function(following){
						return following.toString() === user._id.toString();
					});

					var friendRequest = _.find(req.user.friendRequests, function(friendReq){
						return friendReq.toString() === user._id.toString();
					});

					user.isFriend = foundUser ? 1 : 0;
					user.isPendingRequest = pendingRequest ? 1 : 0;
					user.isFollowingUser = followingUser ? 1 : 0;
					user.isWaitingForConfirmation = friendRequest ? 1 : 0;

					res.send(user);
				}
			});

		},
		generateRSAKeys: function(req, res, next){

			// var keyData = '-----BEGIN PUBLIC KEY----- ... -----END PUBLIC KEY-----';
			// key.importKey(keyData, 'pkcs8');
			// var privateDer = key.exportKey('pkcs1-der');
			var key = new NodeRSA({b: 512});
			pair = key.generateKeyPair(2048, 65537);
			var public = key.exportKey('public');
			console.log(public);

			var rsaPair = new RSAKey();
			rsaPair.publicKey = key.exportKey('public');
			rsaPair.privateKey = key.exportKey('pkcs8');
			rsaPair.os = 'ios';
			rsaPair.save(function(err){
				res.send(err);
			});

			var encrypted = key.encrypt("testing", 'base64');


			// var privateKeyData = pair.exportKey('pkcs1-der');
			// var decryptKey = new NodeRSA();
			// decryptKey.importKey(privateKeyData, 'pkcs1-der');


			// var decrypted = decryptKey.decrypt(encrypted, 'utf8');
			// console.log('decrypted', decrypted);

			// req.user.validPassword(req.body.password);
		},
		testEncryption: function(req, res, next){
			var privateKey = '-----BEGIN PRIVATE KEY-----\nMIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQCsptmb7lR4RsnF\nD4hgcFpbrod6eMjjOVqtquL8rEJXWxNNtoIwwTT8L/gM2y8mF1b2wkl9KXpLgOOM\n24QYUJ7tltFklGqxwZ1XW+irEhvd/9X/ogn9ZY/EelYbpkeWB787jU8Td4FS/Qrq\nGPY3zV44YXsw0vzr0uYdJA6dqgUJK5+Jku/IPL1C44Hlqvu6DFJIIjM8zWdeMQrj\n/7xPPW0vr81euXz7gee0b+avW4q0mIjEjYOEcYdo+itS45BfUKIh0Wk6TsFE+MGY\nSYwbPMZYV6J+0p/fiT1s9/wQsnedONI/gONTmoFVe5KgUH++v6EsGTx+yk5crxCN\nFZzGxJw5AgMBAAECggEBAJC1YFsTzXTqUB99P18ljHafuCnspuPcpKY/PvVtJEW3\nrlxKhqB6sABX7F5KMCCLG3bDrXjclQZHx3G+Y99FY4/cAzF8PFk04L3h6QfJq9XM\nov8aP6lFwNR7fUS7f/C+GYi+jfLRDXITaVyBA44N8dTrz6dCkH7QS9NM73syj/n4\nVVjxWKGAO0+CX4+w8yclz52rXofSwnOoH5fFdmEVDuw6uOxxxI/517FYctVjoPai\n4wvOAJUKr8K57xdMg1+XttXModgSuiaZf0KrNTunqDr0pkX2gX3hzTZ50odOIbwI\nprJ887NC1sFm/G/oDcsZE0qKIdIp4dfWj99YzlIGCaECgYEA7NFJWyxYgTdX3CWg\ns6KOyE/lQessFZ/oEZsl/xfQ50qw6ivAljttMc3QELB9YLUSZ7uThAR2y+3wPbOj\nRxQDurDm36Hdh0aPtSmpyhkdX9zsu4rci7S+2jVPNo+yZ4XxRZMvYbucJZgaci90\nelVfKYld2hov2pdwEspcS3HRHI0CgYEAuqMBI9lz0e/C3n7uUJeOSP7oQsdIVLSW\nrT6mBDYv1ZLG9NaWzY6VsglVvimjuFPvaSOEivuuY9ajINXAqSG27YxvITTyoNgm\nQ7uJ03smrjabMDQYWFuBUyf3n0E5I7hYAcRXEei4SImX6B6XJmcGHhTs+Fs/GPeR\naJHjZN+/cV0CgYEAuDr4fl05XQzY5QLAyf6kZWECUQ3K9ee4ejngjVrRwZ1xlDsF\nT9pbUGt0egdTAnDbKKfcUs/GO0+XneiqFRmU7R/B/CfIVvylv6Hx3K89Et6jrCKT\nAHxUHR1ryl5vw+mHkUC3k6iICRcTO9F/I8xWg9HBmXLV15AIOBYFRwv8SeECgYBT\nnU2YcwZ5inICXlmET7HWMCV8LW3RTSqoKQYVovUhOOXbsPvU0cls/tOkI2GO5kO/\nFcN+xKznW7SvtDPhUhJzMFRlSyRcPRo/hBiGX4j/ycUS/nPdufkheSlt9rmTsDX1\nbLIZeup8zO/6dSIAeW17MqLLzY1iMR+6+okmmztYnQKBgQCqmpJgVBAKnzFB4sJd\nrz32YlsTGc3H90qxm3zObMafXO6WaxfmaBhmi8HrL069ReQdIdUq9j55ZxIBMD0t\nCYGP/bctL2C9Txavchn4+U4hyvKq09CIAgxUxK8vO+55MvtpTqWRkRcYGG+dq/+A\nv7YStPffTIz8Ryso59gToWBovw==\n-----END PRIVATE KEY-----';
			var publicKey = '-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArKbZm+5UeEbJxQ+IYHBa\nW66HenjI4zlarari/KxCV1sTTbaCMME0/C/4DNsvJhdW9sJJfSl6S4DjjNuEGFCe\n7ZbRZJRqscGdV1voqxIb3f/V/6IJ/WWPxHpWG6ZHlge/O41PE3eBUv0K6hj2N81e\nOGF7MNL869LmHSQOnaoFCSufiZLvyDy9QuOB5ar7ugxSSCIzPM1nXjEK4/+8Tz1t\nL6/NXrl8+4HntG/mr1uKtJiIxI2DhHGHaPorUuOQX1CiIdFpOk7BRPjBmEmMGzzG\nWFeiftKf34k9bPf8ELJ3nTjSP4DjU5qBVXuSoFB/vr+hLBk8fspOXK8QjRWcxsSc\nOQIDAQAB\n-----END PUBLIC KEY-----';

			var encryptKey = new NodeRSA(publicKey);
			encryptKey.encrypt("testing", 'base64');

		},
		testKey: function(req, res, next){
			
			var data = req.body.data;
			console.log(data);

			var result = decrypt(data, "PVoC5z9fsZdwWK4Xvc2pjXtiNq5BfGBK");
			console.log(result);

/*			var decryptKey = new NodeRSA(req.body.privateKey);
			// decryptKey.importKey(req.body.privateKey, 'pkcs8');
			var decrypted = decryptKey.decrypt(data, 'base64');
			console.log('decrypted', decrypted);



			var encryptKey = new NodeRSA(req.body.privateKey);
			encryptKey.importKey(req.body.publicKey, 'public');
			var encrypted = encryptKey.encrypt('testing', 'base64');
			console.log(encrypted);
			var decrypted = encryptKey.decrypt(encrypted, 'utf8');
			console.log('decrypted: ', decrypted);
			*/
			next();


		},
		findBraintreeUser: function(req, res, next){
			gateway.customer.find(req.user.braintreeCustomerId, function(err, customer) {
				if (err) {next(err);}
				else{
					if (customer.paypalAccounts.length == 0) {
						next(new Error("USER_HAS_NO_PAYPAL_ACCOUNTS"));
					}else{
						console.log(customer)
						req.braintreeInfo = customer;
						next();
					}
				}
			});
		},
		fetchFavourite: function(req, res, next){

			var pageLimit = 20;
			var page = req.query.page ? req.query.page : 0;
			Meal.find({_id: {$in: req.user.favourites}})
			.skip(20 * req.query.page)
			.limit(20)
			.populate('restaurant', 'restaurantName currency')
			.select('name availableDate productImage availableDate availableEndDate priceInLocalCurrencyString currencyCode productQuantity restaurant')
			.lean()
			.exec(function(err, favorites){
				if (err) {next(err);}
				else{
					async.each(favorites, function(fav, cb){
						fav.isFavourited = _.find(req.user.favourites, function(favorite){return favorite.toString() === fav._id.toString()});
						cb(null);
					}, function(err){
						res.send(favorites);
					});
				}
			});
		},
		addFavourite: function(req, res, next){

			Meal.findById(req.body.mealId).exec(function(err, meal){
				if (err) {next(err);}
				else{
					User.findByIdAndUpdate(req.user._id, {$addToSet: {favourites: meal}}, function(err){
						if (err) {res.send(200)}
							else{
								res.sendStatus(200);
							}

						})
				}
			})

		},
		removeFavourite: function(req, res, next){
			User.findByIdAndUpdate(req.user._id, {$pull: {favourites: mongoose.Types.ObjectId(req.query.mealId)}}, function(err){
				if (err) {res.send(200)}
					else{
						res.sendStatus(200);
					}
				});

		}

	}
}


function decrypt(text, password){
	var decipher = crypto.createDecipher(algorithm,password)
	var dec = decipher.update(text,'hex','utf8')
	dec += decipher.final('utf8');
	return dec;
}



function sendSupportEmail (user, html, subject, callback){

	var data = {
		    //Specify email data
		    from: 'Shanon Support <support@getshanon.com>',
		    //The email to contact
		    to: user.local.email,
		    //Subject and text data  
		    subject: subject,
		    html: html
	}

    //Invokes the method to send emails given the above data with the helper library
	var mailgun = new Mailgun({apiKey: process.env.mailgun_API_key, domain: "getshanon.com"});

    mailgun.messages().send(data, function (err, body) {
        //If there is an error, render the error page
        if (err) {callback(err);}
        else{
        	callback(null);
        }

    });

}


// userSettings = function(self, user){

// 	async.each(users, function(user, cb){

// 		var foundUser = _.find(self.friends, function(user){
// 			return user.toString() === req.query.userId
// 		});

// 		var pendingRequest = _.find(self.pendingFriendRequests, function(user){
// 			return user.toString() === req.query.userId
// 		});

// 		var followingUser = _.find(self.following, function(user){
// 			return user.toString() === req.query.userId
// 		});

// 		user.isFriend = foundUser ? 1 : 0;
// 		user.isPendingRequest = pendingRequest ? 1 : 0;
// 		user.isFollowingUser = followingUser ? 1 : 0;
// 		cb(null);

// 	}, function(err){
// 		console.log(users);
// 	});

// };
