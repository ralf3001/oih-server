var _ = require('underscore'),
	async = require('async')
	CompetitionEntry = require('./model')



module.exports = function () {
	
	return {

		create: function(req, res, next){

			var entry = new CompetitionEntry();
			
			entry.content = req.body.content;
			entry.imageURL = req.body.imageURL;
			entry.geometry.coordinates = req.body.coordinates.split(',').map(Number);
			entry.geometry.type = 'Point';
			entry.competition = req.competition.id;
			entry.user = req.user;

			entry.save(function(err, e){
				if (err) {next(err);}
				else{
					res.send(e);
				}
			});

		},
		fetch: function(req, res, next){

			CompetitionEntry.find({user: req.query.userId}).sort('-createDate').limit(1).lean().exec(function(err, competition){
				if (err) {next(err);}
				if (!competition) {next(new Error("NO_COMPETITION_ENTRY"));}
				res.send(competition[0]);
				
			});

		},
		vote: function(req, res, next){
			
			CompetitionEntry.findByIdAndUpdate(req.body.entryId, {$inc: {voteCount: 1}}, function(err){
				if (err) {next(err);}
				else{
					res.sendStatus(200);
				}
			});

		},
		frontRunners: function(req, res, next){
			
			CompetitionEntry.find({competition: req.competition}).sort('-voteCount').limit(10).lean().exec(function(err, entries){
				if (err) {next(err);}
				else{
					res.send(entries);
				}
			});

		}

	}

}