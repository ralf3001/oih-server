var Restaurant = require('../server/restaurant/model');
var User = require('../server/models/user'),
	async = require('async'),
	geocoder = require('geocoder'), 
	utf8 = require('utf8'),
	sleep = require('sleep'),
	_ = require('underscore'),
	paypal = require('paypal-rest-sdk')

paypal.configure({
  'mode': 'sandbox', //sandbox or live
  'client_id': 'AXSVxneP8mUHKxNqoUcvA7j6VupexuBQVHQDuiYf56qEZNMqZDuyb9BO9llJUO6cLvx-VXnWeeQelC25',
  'client_secret': 'EDBNnfHuNR_9g5zlltFx80zWiObqEyjR2xNW1Zc1B2f0jz3otTPBTl1hHuXJ70jchifzjQZ7eCjm7aQ5'
});



// var card_data = {
//   "type": "visa",
//   "number": "4417119669820331",
//   "expire_month": "11",
//   "expire_year": "2018",
//   "cvv2": "123",
//   "first_name": "Joe",
//   "last_name": "Shopper"
// };

// paypal.creditCard.create(card_data, function(error, credit_card){
//   if (error) {
//     console.log(error);
//     throw error;
//   } else {
//     console.log("Create Credit-Card Response");
//     console.log(credit_card);
//   }
// })





var locations = [
	// {
	// 	addressString: "Leader Industrial Centre, 57 Au Pui Wan Street, Hong Kong",
	// 	name: 'Fooody',
	// 	phoneNumber: '3586 0863'
	// },
	// {
	// 	addressString: "Tao Miao Institute, 13 Au Pui Wan Street, Hong Kong",
	// 	name: 'Tao Heung Training Restaurant',
	// 	phoneNumber: '8300 8007'
	// },
	// {
	// 	addressString: "Goodman Shatin Logistics Centre Phase 2, 6 Wong Chuk Yeung Street, Hong Kong",
	// 	name: 'Pizza-Box',
	// 	phoneNumber: '2515 1515'
	// },
	// {
	// 	addressString: 'On Wah Industrial Building, 41-43 Au Pui Wan Street, Hong Kong',
	// 	name: 'K-Pop snack',
	// 	phoneNumber: '2446 3336'
	// },
	// {
	// 	addressString: '18-24 Shan Mei St, Hong Kong',
	// 	name: 'Gourmet Coffee',
	// 	phoneNumber: '3188 4395'
	// },
	// {
	// 	addressString: '63 Bonham Road, Hong Kong',
	// 	name: 'Tulsi & Wine',
	// 	phoneNumber: '2517 7077'
	// },
	// {
	// 	addressString: '43 High Street, Hong Kong',
	// 	name: 'Pizzeria Italia',
	// 	phoneNumber: '2525 2519'
	// },
	// {
	// 	addressString: '62 High Street, Hong Kong',
	// 	name: 'La Terra',
	// 	phoneNumber: '2858 8173'
	// },
	// {
	// 	addressString: 'Wo Yick Mansion, 263 Queen\'s Road West, Hong Kong',
	// 	name: 'Kwan Kee Claypot Rice',
	// 	phoneNumber: '2803 7209'
	// },
	// {
	// 	addressString: '49 Bonham Road, Hong Kong',
	// 	name: 'El Loco Gringo',
	// 	phoneNumber: '2858 8833'
	// }
	{
		addressString: '50 High St, Hong Kong',
		name: 'Czarina 1964',
		phoneNumber: '2540 2874'
	},
	{
		addressString: 'Olympian City, 18 Hoi Ting Road, Hong Kong',
		name: 'Tim Ho Wan'
	},
	{
		addressString: '55 Dundas Street, Hong Kong',
		name: '肥姐小食店'
	}
]


// async.each(data, function(item, cb) {
//   Person.create(item, cb);
// }, function(err) {
//   if (err) {
//     // handle error
//   }

//5644503eb0bd602b14bac6ca
geocoder.selectProvider("google");


/*
User.findById('56713377222c50656683dc0a').exec(function(err, u){
	if (err) {return; }
	else{
		async.each(locations, function(l, cb){


			console.log(l.name);

			var restaurant = new Restaurant();
			restaurant.restaurantName = utf8.encode(l.name);
			restaurant.accountsAssociatedTo.push(u);

			geocodeRestaurant(u, l, restaurant, l.addressString, function(err){
				cb(err);
			});


		}, function(err){
			if (err) {console.log(err)}
			else{
				console.log('done');
			}
		});

	}
});



geocodeRestaurant = function(user, location, restaurant, addressString, callback){

	async.waterfall([

		function(callback){
			
			var address = addressString;


			geocoder.geocode(address, function(err, data){
				if (err) {callback(err);}
				else {
					
					console.log(restaurant.restaurantName, data.results[0]);

					var country = _.find(data.results[0].address_components, function(component){
						return _.contains(component.types, 'country');
					});
		

					var location = data.results[0].geometry.location;
					var locationArray = [location.lng, location.lat];
					
					restaurant.geometry.coordinates = locationArray;
					restaurant.geometry.type = 'Point';
					restaurant.address.country = country.long_name; 
					restaurant.addressString = data.results[0].formatted_address;
					restaurant.phoneNumber = location.phoneNumber;
					callback(null, restaurant);
				}
			});					



		},
		function(rest, callback){

			rest.save(function(err){
				callback(err, rest);
			});

		}, function(rest, callback){


			if (rest) {
				user.merchantsOf.push(rest);
				user.save(function(err){
					if (err) {callback(err);}
					callback(null);
				});						
			}else{
				callback(new Error('Cannot add restaurant'));
			}


		}


		], function(err, result){
			callback(err);
	});

}



*/


