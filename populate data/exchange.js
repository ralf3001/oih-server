var oxr = require("open-exchange-rates"),
	fx = require("money"),
	async = require('async'),
	ExchangeRate = require('../server/exchange-rate/model')


oxr.set({app_id: 'a040d478fe6246b28d748fd6654f5ba2'});


async.waterfall([
	function(callback){
		oxr.latest(function() {
			callback();
		});
	},function(callback){
		
		console.log(oxr);

		var ex = new ExchangeRate();
		ex.data = oxr.rates;
		ex.updated = oxr.timeStamp;
		ex.base = oxr.base;
		ex.save(function(err){
			callback(err);
		});

		// console.log(fx(100).from('HKD').to('GBP')); // ~8.0424

	}

	], function(err, result){
		console.log('done');
});

// async.waterfall([
// 	function(callback){
// 		oxr.latest(function() {
// 			callback();
// 		});
// 	},function(callback){

// 		console.log(oxr.rates);

// 		fx.rates = oxr.rates;
// 		fx.base = oxr.base;
	
// 		console.log(fx(100).from('HKD').to('GBP')); // ~8.0424

// 	}

// 	], function(err, result){
// 		console.log('done');
// });


// oxr.latest(function() {

// 	fx.rates = oxr.rates;
// 	fx.base = oxr.base;
	
// 	// money.js is ready to use:
// 	console.log(fx(100).from('HKD').to('GBP')); // ~8.0424

// });