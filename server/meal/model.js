var mongoose = require('mongoose'),
	connection = require('../../config/userDBConnection'),
	restaurant = require('../restaurant/model');


var mealSchema = mongoose.Schema({

	type: {type: String, enum: ['breakfast', 'lunch', 'dinner', 'snack']},
	availableDate: Date,
	availableEndDate: Date,
	name: {type: String, required: true},
	priceInUSD: Number,
	priceInLocalCurrency: Number,
	priceInLocalCurrencyString: String,
	currencyCode: String,
	restaurant: {type:mongoose.Schema.Types.ObjectId, ref: 'Restaurant'},
	lastUpdate: Date,
	orderRequiredToDeliver: Number,
	orderProcessed: Number,
	productImage: String,
	productIngredients: String,
	productNetWeightInString: String,
	productQuantity: Number,
	productNote: String,

	subscription: {type: mongoose.Schema.Types.ObjectId, ref: 'subscription'}	//subscription the item belongs to


});


var mealModel = connection.model('Meal', mealSchema);
module.exports = mealModel;
