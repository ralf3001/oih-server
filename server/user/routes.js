module.exports = function (app, auth) {


	var user = require('./controller')();

	app.route('/user')
		.get(auth.isLoggedIn, user.findUserByEmail);

	app.route('/user/:id')
		.get(auth.isLoggedIn, user.fetchUser);

	app.route('/deviceToken')
		.post(auth.isLoggedIn, user.addNotificationToken)
		.put(auth.isLoggedIn, user.deleteNotificationToken);

	app.route('/socialTokens')
		.post(auth.isLoggedIn, user.socialTokens);

	app.route('/profilepic')
		.post(auth.isLoggedIn, user.addprofilepic);
	
	app.route('/profile')
		.put(auth.isLoggedIn, user.updateProfile);

	app.route('/fbfriends')
		.get(auth.isLoggedIn, user.fbfriends);

	app.route('/favourites')
		.get(auth.isLoggedIn, user.fetchFavourite)
		.post(auth.isLoggedIn, user.addFavourite)
		.delete(auth.isLoggedIn, user.removeFavourite);

	app.route('/generateRSAKeys')
		.post(auth.isLoggedIn, user.generateRSAKeys);

	app.route('/testKey')
		.post(auth.isLoggedIn, user.testKey);

	app.route('/reset')
		.get(user.checkresetPassword)
		.post(user.resetPassword)

	app.route('/forgot')
		.post(user.forgotpw);
		

	// app.param('restaurantId', restaurants.restaurant);

}