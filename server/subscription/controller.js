var	_ = require('underscore'),
	geocoder = require('geocoder'),
	async = require('async'),
	braintree = require("braintree"),
	cc = require('currency-codes'),
	utf8 = require('utf8'),
	Order = require('./../order/model'),
	Activity = require('./../activity/model'),
	SalesSchema = require('./../salesMapReduce/model'),
	oxr = require("open-exchange-rates"),
	fx = require("money"),
	User = require('./../models/user'),
	Subscription = require('./model')



module.exports = function () {
	
	return {
		create: function(req, res, next, id){
			
			var sub = new Subscription();
			sub.user = req.user;
			sub.subscriptionName = req.body.subscriptionName;
			sub.subscriptionDescription = req.body.subscriptionDescription;
			sub.priceInLocalCurrencyString = req.body.priceInLocalCurrency.toString();
			sub.priceInLocalCurrency = req.body.priceInLocalCurrency;
			// sub.currencyCode =  

			sub.save(function(err){
				if (err) {next(err);}
				else{
					res.sendStatus(200);
				}
			});

		},
		subscribe: function(req, res, next){
			
			var sub = req.subscription;
			req.user.subscribing.push(sub.id);

			req.user.save(function(err){
				if (err) {next(err);}
				else{
					next();
				}
			});

		},
		unsubscribe: function(req, res, next){

			var sub = req.subscription;
			req.user.subscribing.pull(sub.id);

			req.user.save(function(err){
				if (err) {next(err);}
				else{
					next();
				}
			});

		},
		subscription: function(req, res, next, id){

			Subscription.findById(id, function(err, subscription){
				if (err) {return next(err);}
				if (!subscription) {return next(new Error("NO_SUCH_SUBSCRIPTION"))}
				else{
					req.subscription = subscription;
					next();
				}
			})

		},
		user: function(req, res, next){
			
			var oDate = req.query.createdAt ? req.query.createdAt : Date.now();

		Subscription.find({user: req.params.userId, createdAt: {$gte: oDate} }, function(err, subscriptions){
				if (err) {next(err);}
				else{
					res.send(subscriptions);
				}
			});

		}

	}

}