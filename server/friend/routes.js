module.exports = function (app, auth) {
	// body...

	var friends = require('./controller')();
		
	app.route('/friend')
		.post(auth.isLoggedIn, friends.user, friends.add)
		.get(auth.isLoggedIn, friends.listFriends)
		.delete(auth.isLoggedIn, friends.remove);

	app.route('/pendingRequests')
		.get(auth.isLoggedIn, friends.listPending)
		.put(auth.isLoggedIn, friends.removePendingRequest);

	app.route('/friendRequests')
		.get(auth.isLoggedIn, friends.listRequests);

	app.route('/checkIfFriend')
		.get(auth.isLoggedIn, friends.checkFriend);

	app.route('/friendsinfo')
		.get(auth.isLoggedIn, friends.friendsCount);

	app.route('/follow')
		.post(auth.isLoggedIn, friends.user, friends.follow)
		.delete(auth.isLoggedIn, friends.unfollow)

	app.route('/follower')
		.get(auth.isLoggedIn, friends.fetchFollower)

	app.route('/following')
		.get(auth.isLoggedIn, friends.fetchFollowing)

	app.route('/confirmFriend')
		.get(auth.isLoggedIn, friends.confirmFriend)
	// app.param('userId', friends.user);
}