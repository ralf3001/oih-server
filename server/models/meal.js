var mongoose = require('mongoose'),
	connection = require('../../config/userDBConnection'),
	restaurant = require('./restaurant');


var mealSchema = mongoose.Schema({

	type: {type: String, enum: ['breakfast', 'lunch', 'dinner', 'snack']},
	availableDate: Date,
	name: String,
	priceInUSD: Number,
	priceInLocalCurrency: Number,
	priceInString: String,
	restaurant: {type:mongoose.Schema.Types.ObjectId, ref: 'Restaurant'}

});


var mealModel = connection.model('Meal', mealSchema);
module.exports = mealModel;
