var mongoose = require('mongoose'),
	connection = require('../../config/userDBConnection')



var conversationSchema = mongoose.Schema({

	users: [{type:mongoose.Schema.Types.ObjectId, ref: 'User'}],
	createDate: {type: Date, default: Date.now},
	conversationName: {type: String}
});


var conversationModel = connection.model('ConversationBlock', conversationSchema);
module.exports = conversationModel