var mongoose = require('mongoose'),
	connection = require('../../config/userDBConnection'),
	user = require('../models/user')

var competitionSchema = mongoose.Schema({

	createdAt: {type: Date, default: Date.now},
	endedAt: {type: Date, default: Date.now},
	competitionTitle: String,
	competitionDescription: String,
	
  	voters: [{type: mongoose.Schema.Types.ObjectId, ref: 'User'}],
  	dismissedUsers: [{type: mongoose.Schema.Types.ObjectId, ref: 'User'}]

});

competitionSchema.index({geometry: '2dsphere'});


var orderModel = connection.model('Competition', competitionSchema);
module.exports = orderModel