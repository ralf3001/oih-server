var oxr = require("open-exchange-rates"),
	fx = require("money"),
	async = require('async'),
	ExchangeRate = require('./model'),
	CronJob = require('cron').CronJob

oxr.set({app_id: 'a040d478fe6246b28d748fd6654f5ba2'});



module.exports = function () {

	return{

		fetch: function(){

			var job = new CronJob({
			  cronTime: '00 39 23 * * *',
			  onTick: function() {

				async.waterfall([
					function(callback){
						oxr.latest(function() {
							callback();
						});
					},function(callback){

						var ex = new ExchangeRate();
						ex.data = oxr.rates;
						ex.updated = oxr.timeStamp;
						ex.base = oxr.base;

						ex.save(function(err){
							callback(err);
						});


					}

					], function(err, result){

				});

			     // * Runs every weekday (Monday through Friday)
			     // * at 11:30:00 AM. It does not run on Saturday
			     // * or Sunday.
			     
			  },
			  start: true,
			  timeZone: 'Asia/Hong_Kong'
			});
			job.start();


		},
		exchange: function(currency, amount, callback){
			async.waterfall([
				function(callback){
					ExchangeRate.find({}).lean().sort('-updated').exec(function(err, result){
						callback(err, result[0]);
					});
				},
				function(ex, callback){

					fx.base = ex.base;
					fx.rates = ex.data;
					if (!amount) {return callback(new Error("AMOUNT_ERROR"));}
					if (!currency) {return callback(new Error("CURRENCY_ERROR"));}
					callback(null, fx(amount).from(currency).to('HKD'));
				}	

				], function(err, result){
					callback(err, result);
			});

		}


	}

};

// async.waterfall([
// 	function(callback){
// 		oxr.latest(function() {
// 			callback();
// 		});
// 	},function(callback){

// 		console.log(oxr.rates);

// 		fx.rates = oxr.rates;
// 		fx.base = oxr.base;
	
// 		console.log(fx(100).from('HKD').to('GBP')); // ~8.0424

// 	}

// 	], function(err, result){
// 		console.log('done');
// });
