var mongoose = require('mongoose').Schema,
	connection = require('../../config/userDBConnection'),
	order = require('../order/model'),
	user = require('../models/user');

var activitySchema = mongoose({

	fromUser: {type: mongoose.Types.ObjectId, ref: 'User'},
	restaurant: {type: mongoose.Types.ObjectId, ref: 'Restaurant'},
	toUser: {type: mongoose.Types.ObjectId, ref: 'User'},
	activityType: {type: String, enum: ['orderFood', 'like', 'review', 'thank', 'newItem', 'comment']},
	order: {type: mongoose.Types.ObjectId, ref: 'Order'},
	meal: {type: mongoose.Types.ObjectId, ref: 'Meal'},
	activity:{type: mongoose.Types.ObjectId, ref: 'Activity'},
	content: String,
	createdAt: {type:Date, default: Date.now},
	postScore: Number,
	postPicture: String,
	rating: Number,
	audioNote: String
	
});

var model = connection.model('Activity', activitySchema);
module.exports = model;