var Receipt = require('./model'),
	_ = require('underscore'),
	paypal = require('paypal-rest-sdk'),
	braintree = require("braintree"),
	gateway = braintree.connect({
	  environment: braintree.Environment.Sandbox,
	  merchantId: process.env.braintreeMerchantId,
	  publicKey: process.env.braintreePublicKey,
	  privateKey: process.env.braintreePrivateKey
	});


	paypal.configure({
	  'mode': 'sandbox', //sandbox or live
	  'client_id': 'AXSVxneP8mUHKxNqoUcvA7j6VupexuBQVHQDuiYf56qEZNMqZDuyb9BO9llJUO6cLvx-VXnWeeQelC25',
	  'client_secret': 'EDBNnfHuNR_9g5zlltFx80zWiObqEyjR2xNW1Zc1B2f0jz3otTPBTl1hHuXJ70jchifzjQZ7eCjm7aQ5'
	});


module.exports = function  () {
	return {

		create: function(req, res, next){
			
			gateway.transaction.sale({

				amount: req.body.totalChargeInString,
				paymentMethodNonce: req.body.paymentNounce,
				customer:{
					id: req.user.brainTreeCustomerId
				},
				options:{
					storeInVaultOnSuccess: true
				}

			}, function(err, result){
				if (err) {next(err);}
				else{
					if (result.success) {

						var receipt = new Receipt();

						receipt.totalAmount = Number(req.body.totalChargeInString);
						receipt.receiptId = result.transaction.id;
						receipt.save(function(err){
							if (err) {console.log("ERROR saving receipt"); next(err);}
							else{
								req.receipt = receipt;
								next();
							}
						});
						
					}else{
						next(new Error(result.message));
					}


				}
			});
		}, 
		receipt: function(req, res, id){

			Receipt.findById(id).exec(function(err, receipt){
				if (err) {next(err);}
				if (!receipt) {next(new Error("can't find this receipt"))}
				req.receipt = receipt;
				next();
			});
		},
		showUserReceipts: function(req, res){
			
			Receipt.find({user: req.user}, function(err, receipts){
				if (err) {next(err);}
				res.send(receipts);
			});
		
		},
		cancelTransaction: function(req, res, next){
			gateway.trasaction.void(req.transactionId, function(err, result){
				if (result.success) {return next();}
				else return next(result.message);
			});
		}

	}
}
