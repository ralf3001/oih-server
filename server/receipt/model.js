var mongoose = require('mongoose');
var connection = require('../../config/userDBConnection');
var User = require('../models/user'),
	Meal = require('../meal/model')

//Receipt Schema

var receiptSchema = mongoose.Schema({

	receiptId: {type: String, required: true},
	refundId: String,
	status: String,
	order: {type: mongoose.Schema.Types.ObjectId, ref: 'Order'},
	totalAmount: Number
});



var receiptModel = connection.model('Receipt', receiptSchema);
module.exports = receiptModel;
