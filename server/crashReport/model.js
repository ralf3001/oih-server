var mongoose = require('mongoose');
var User = require('../models/user');
var connection = require('../../config/crashReportConnection');

var giftcodeSchema = mongoose.Schema({

    crashReason   :{
        type: String
    },
    callStackSymbols    :{
        type: [String]
    },
    systemVersion :{
        type: Number
    },
    version :{
        type: String
    },
    build   :{
        type: String
    },
    IPaddress:{
        type: String
    },
    bug :{
        type: String
    },
    user:{
        type: mongoose.Schema.Types.ObjectId, ref: 'User'
    },
    timeStamp : {type: Date},
    createDate: {type: Date, default: Date.now()},
    model : {type: String},
    OS:{type: String}

});

var giftcodeModel = connection.model('CrashReport', giftcodeSchema);
module.exports = giftcodeModel;