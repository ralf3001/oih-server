module.exports = function (app, auth) {
	// body...

	var cart = require('./controller')();

	app.route('/cart')
		.post(auth.isLoggedIn, cart.add)
		.get(auth.isLoggedIn, cart.fetch)
		.delete(auth.isLoggedIn, cart.delete)		

}