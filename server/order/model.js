var mongoose = require('mongoose'),
	connection = require('../../config/userDBConnection'),
	meal = require('../meal/model'),
	restaurant = require('../restaurant/model'),
	user = require('../models/user')



var orderSchema = mongoose.Schema({

	sellerRestaurant: {type:mongoose.Schema.Types.ObjectId, ref: 'Restaurant'},
	orderDate: {type: Date, default: Date.now},
	orderUser: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
	recipientUser: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
	recipientAddress: {type: mongoose.Schema.Types.ObjectId, ref: 'Address'},
	orderMeal: {type: mongoose.Schema.Types.ObjectId, ref: 'Meal'},
	isDelivered: {type: Boolean, default: false},
	discount: Number,
	isAccepted: {type: Boolean, default: false},
	isGift: Boolean,
	orderStatus: {type: String, enum: ['ordered', 'accepted', 'delivered', 'canceled'], default: 'ordered'},
	paymentMethod: String,
	orderReceipt: {type: mongoose.Schema.Types.ObjectId, ref: 'Receipt'},
	quantity: Number,
	orderAmount: Number,
	orderAmountInString: String,
	comment: String,
	isAcknowledgedByRecipient: {type: Boolean, default: false},
	orderReview: {type: mongoose.Schema.Types.ObjectId, ref: 'Activity'},
	audioReview: String,
	orderMessage: String
	
});


var orderModel = connection.model('Order', orderSchema);
module.exports = orderModel