module.exports = function (app, auth) {


	var conversation = require('./controller')();
	var message = require('../conversationMessage/controller')();

	app.route('/conversation')
		.get(auth.isLoggedIn, conversation.userConversations, message.lastMessage)
		.post(auth.isLoggedIn, conversation.create, message.create);

	app.route('/conversation/:userId')
		.get(auth.isLoggedIn, conversation.fetchIndividualConversation, message.fetch)

}