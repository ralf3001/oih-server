var Restaurant = require('./models/restaurant')


var K = 20;
var PERF = 400;

Elo.prototype.calculateScore = function(restaurants, winner) {
	// body...

	restaurants.append(winner);
	var winnerScore = restaurants.count - 1;
	var loserScore = (restaurants.count - 1) * 0.5 -1;
	var restaurantArrayCopy = restaurants.slice();

	for (var i = 0; i < restaurants.length; i++) {
		
		var expected = expectedScore(restaurants restaurantArrayCopy[i]);
		if (restaurantArrayCopy[i].name == winner.name) {
			restaurants[i].score = (restaurants[i].score + K * (winnerScore -expectedScore))
		}else{
			restaurants[i].score = (restaurants[i].score + K * (loserScore - expectedScore))

		}
	};

	return restaurants;
};


Elo.prototype.expectedScore = function(restaurants, restaurant){

	var score = 0;

	for (var i = 0; i < restaurants.length; i++) {
		if (restaurant.name != restaurants[i].name) {
			score += calculateExpectedScore(restaurant.score, restaurants[i].score);
		}
	};

	return score;
}

Elo.prototype.calculateExpectedScore = function(score1, opponentRating){

  return 1 / (1 + Math.pow(10, (opponentRating - score1))/PERF));

}

