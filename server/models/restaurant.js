var mongoose = require('mongoose');
var User = require('./user');
var connection = require('../../config/userDBConnection');

var restaurantSchema = mongoose.Schema({

	restaurantName: String,
	address: {
		addressLine1: String,
		addressLine2: String,
		city: String,
		zipcode: String,
		country: String
	},
	geometry: {
		type: {type: String},
		coordinates: []
	   // coordinates: { coordin: [Number], index: '2dsphere'}
	},
	addressString: String,
	
	eloScore: {type: Number, default: 1000},

  	accountsAssociatedTo: [{type: mongoose.Schema.Types.ObjectId, ref: 'User'}],
	meal: [{type: mongoose.Schema.Types.ObjectId, ref: 'Meal'}]


});


restaurantSchema.index({geometry: '2dsphere'});


// mongooseRedisCache(mongoose)

// mongooseRedisCache(mongoose, {
//    host: "redisHost",
//    port: "redisPort",
//    pass: "redisPass",
//    options: "redisOptions"
//  });




var RestaurantModel = connection.model('Restaurant', restaurantSchema);
module.exports = RestaurantModel;

