// load all the things we need
var LocalStrategy    = require('passport-local').Strategy;
var TokenStrategy = require('passport-token').Strategy;
var mongoose = require('mongoose');
// var stripe = require('stripe')("sk_test_iQqzUXNlZSigN6k9ovd8LY51");
var flash    = require('connect-flash'),
    validator = require('validator'),
    crypto = require('crypto'),
    algorithm = 'aes-256-ctr';


    
var dbConnection = require('./userDBConnection');

var strategyOptions = {
    usernameHeader: 'x-custom-username',
    tokenHeader:    'x-custom-token',        
    usernameField:  'custom-username',
    tokenField:     'custom-token',
    passReqToCallback : true 

};

var braintree = require("braintree"),
    gateway = braintree.connect({
      environment: braintree.Environment.Sandbox,
      merchantId: process.env.braintreeMerchantId,
      publicKey: process.env.braintreePublicKey,
      privateKey: process.env.braintreePrivateKey
    });


// load up the user model
var User = dbConnection.model('User');

var async = require('async');


var GiftCode = require('../server/coupon/model');

function encrypt(text){
  var cipher = crypto.createCipher(algorithm,password)
  var crypted = cipher.update(text,'utf8','hex')
  crypted += cipher.final('hex');
  return crypted;
}
 
function decrypt(text, password){
  var decipher = crypto.createDecipher(algorithm,password)
  var dec = decipher.update(text,'hex','utf8')
  dec += decipher.final('utf8');
  return dec;
}


module.exports = function(passport) {

    // =========================================================================
    // passport session setup ==================================================
    // =========================================================================
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    // used to deserialize the user
    passport.deserializeUser(function(id, done) {

        User.findById(id).populate('address').exec(function(err, user){
            if (err) {return done(err);}
            if (user){
                if (!user.accessToken) {
	                var token = user.generateRandomToken();
                    user.accessToken = token;
		            user.save(function(err, u){
		                return done(null, u);
		            });

                }else{
                	return done(null, user);
                }
            }
        });

    });

    // =========================================================================
    // LOCAL LOGIN =============================================================
    // =========================================================================
    passport.use('local-login', new LocalStrategy({

        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true 
    },
    function(req, email, password, done) {


        process.nextTick(function() {

            User.findOne({'local.email' :  email}, function(err, user) {

                if (err)
                    return done(err);

                if (!user)
                    return done(null, false, req.flash('loginMessage', 'No user found.'));

                if (!user.validPassword(password))
                    return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.'));
                else{
                    return done(null, user);
                }
            });
        });

    }));

    passport.use('local-login-token', new TokenStrategy(strategyOptions,
    function(req, username, token, done) {

        process.nextTick(function() {
            
            User.findOne({ 'accessToken' :  token, 'local.email': username}, function(err, user) {
                if (err)
                    return done(err);

                if (!user)
                    return done(null, false, req.flash('loginMessage', 'No user found.'));
                else{
                    return done(null, user);
                }
            });
        });

    }));

    // =========================================================================
    // LOCAL SIGNUP ============================================================
    // =========================================================================
    passport.use('local-signup', new LocalStrategy({

        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true // allows us to pass in the req from our route (lets us check if a user is logged in or not)
    },
    function(req, email, password, done) {

        process.nextTick(function() {

            User.findOne({'local.email': email}, function(err, existingUser) {
                
                if (err){
                    return done(err);
                }
                if (existingUser != null){
                    // console.log('existingUser');
                    return done(null, false, req.flash('signupMessage', 'That email is already taken.'));
                }
                    
                if (!validator.isEmail(email)) {
                    return done(null, false, req.flash('signupMessage', "Invalid email address"));
                }
                if (!validator.isLength(password, 6)) {
                    return done(null, false, req.flash('signupMessage', "Password must be at least 3 characters"));
                }

                if(req.user) {
                    var user            = req.user;
                    user.local.email    = email;
                    user.local.password = user.generateHash(password);
                    user.save(function(err) {
                        if (err)
                            throw err;
                        return done(null, user);
                    });
                } 

                else {

                    var newUser            = new User();
                    newUser.name           = req.body.name;
                    newUser.lastName       = req.body.lastName;
                    newUser.local.email    = email;
                    newUser.local.password = newUser.generateHash(password);
                    newUser.activated      = false;
           

                    async.waterfall([
                            function(callback){
                                gateway.customer.create({
                                    firstName: req.body.name,
                                    lastName: req.body.lastName,
                                    email: email
                                }, function (err, result) {
                                    if (err) {callback(err);}
                                    if (result.success) {
                                        callback(null, result);
                                    }
                                });

                            },
                            function(result, callback){

                                newUser.braintreeCustomerId = result.customer.id;
                                newUser.save(function(err, nUser){
                                    callback(err, nUser);
                                });
                            },
                            function(nUser, callback){

                                var code = new GiftCode();
                                code.giftCodeId = code.generateGiftId(req.body.name);
                                code.user = nUser;

                                code.save(function(err, c){
                                    callback(err, nUser);
                                });

                            }

                        ], function(err, results){
                            
                            if (err) {done(err);}
                            else{
                                done(null, results);
                            }


                    });

                    // gateway.customer.create({
                    //   firstName: req.body.name,
                    //   lastName: "testing",
                    //   email: email
                    // }, function (err, result) {

                    //     if (err) {done(err);}
                    //     if (result.success) {

                    //     newUser.save(function(err) {
                    //         if (err)
                    //             throw err;
                    //         return done(null, newUser);
                    //     });

                    //   }
                    //   // e.g. 494019
                    // });



                }

            });
        });

    }));


/*
    // =========================================================================
    // FACEBOOK ================================================================
    // =========================================================================
    passport.use(new FacebookStrategy({

        clientID        : configAuth.facebookAuth.clientID,
        clientSecret    : configAuth.facebookAuth.clientSecret,
        callbackURL     : configAuth.facebookAuth.callbackURL,
        passReqToCallback : true // allows us to pass in the req from our route (lets us check if a user is logged in or not)

    },
    function(req, token, refreshToken, profile, done) {

        process.nextTick(function() {


            if (!req.user) {

                User.findOne({ 'facebook.id' : profile.id }, function(err, user) {
                    if (err)
                        return done(err);

                    if (user) {


                        if (!user.facebook.token) {
                            user.facebook.token = token;
                            user.facebook.name  = profile.name.givenName + ' ' + profile.name.familyName;
                            user.facebook.email = profile.emails[0].value;

                            user.save(function(err) {
                                if (err)
                                    throw err;
                                return done(null, user);
                            });
                        }

                        return done(null, user); // user found, return that user
                    } else {

                        var newUser            = new User();

                        newUser.facebook.id    = profile.id;
                        newUser.facebook.token = token;
                        newUser.facebook.name  = profile.name.givenName + ' ' + profile.name.familyName;
                        newUser.facebook.email = profile.emails[0].value;

                        newUser.save(function(err) {
                            if (err)
                                throw err;
                            return done(null, newUser);
                        });
                    }
                });

            } else {

                var user            = req.user; // pull the user out of the session

                user.facebook.id    = profile.id;
                user.facebook.token = token;
                user.facebook.name  = profile.name.givenName + ' ' + profile.name.familyName;
                user.facebook.email = profile.emails[0].value;

                user.save(function(err) {
                    if (err)
                        throw err;
                    return done(null, user);
                });

            }
        });

    }));
    


    passport.use(new FacebookTokenStrategy({

        clientID        : configAuth.facebookAuth.clientID,
        clientSecret    : configAuth.facebookAuth.clientSecret,
        callbackURL     : configAuth.facebookAuth.callbackURL,
        passReqToCallback : true // allows us to pass in the req from our route (lets us check if a user is logged in or not)

    },
    function(req, token, refreshToken, profile, done) {


        process.nextTick(function() {

            if (!req.user) {
                User.findOne({$or :[{ 'facebook.id' : profile.id }, {'local.email': profile.emails[0].value}]}, function(err, user) {

                // User.findOne({ 'facebook.id' : profile.id }, function(err, user) {
                    if (err)
                        return done(err);

                    if (user) {

                        if (!user.facebook.token) {
                            user.facebook.token = token;
                            user.facebook.name  = profile.name.givenName + ' ' + profile.name.familyName;
                            user.facebook.email = profile.emails[0].value;

                            user.save(function(err) {
                                if (err)
                                    throw err;
                                return done(null, user);
                            });
                        }else{
                            return done(null, user);
                        } // user found, return that user
                    } else {

                        var newUser            = new User();

                        newUser.facebook.id    = profile.id;
                        newUser.facebook.token = token;
                        newUser.facebook.name  = profile.name.givenName + ' ' + profile.name.familyName;
                        newUser.facebook.email = profile.emails[0].value;

                        newUser.save(function(err) {
                            if (err)
                                throw err;
                            console.log('added new fb user');
                            return done(null, newUser);
                        });
                    }
                });

            } else {

                var user            = req.user; // pull the user out of the session

                user.facebook.id    = profile.id;
                user.facebook.token = token;
                user.facebook.name  = profile.name.givenName + ' ' + profile.name.familyName;
                user.facebook.email = profile.emails[0].value;

                user.save(function(err) {
                    if (err)
                        throw err;
                    return done(null, user);
                });

            }
        });

    }));

    // =========================================================================
    // TWITTER =================================================================
    // =========================================================================
    passport.use(new TwitterStrategy({

        consumerKey     : configAuth.twitterAuth.consumerKey,
        consumerSecret  : configAuth.twitterAuth.consumerSecret,
        callbackURL     : configAuth.twitterAuth.callbackURL,
        passReqToCallback : true 

    },
    function(req, token, tokenSecret, profile, done) {

        process.nextTick(function() {

            if (!req.user) {

                User.findOne({ 'twitter.id' : profile.id }, function(err, user) {
                    if (err)
                        return done(err);

                    if (user) {

                        if (!user.twitter.token) {
                            user.twitter.token       = token;
                            user.twitter.username    = profile.username;
                            user.twitter.displayName = profile.displayName;

                            user.save(function(err) {
                                if (err)
                                    throw err;
                                return done(null, user);
                            });
                        }

                        return done(null, user); // user found, return that user
                    } else {
                        // if there is no user, create them
                        var newUser                 = new User();

                        newUser.twitter.id          = profile.id;
                        newUser.twitter.token       = token;
                        newUser.twitter.username    = profile.username;
                        newUser.twitter.displayName = profile.displayName;

                        newUser.save(function(err) {
                            if (err)
                                throw err;
                            return done(null, newUser);
                        });
                    }
                });

            } else {
                var user                 = req.user; // pull the user out of the session

                user.twitter.id          = profile.id;
                user.twitter.token       = token;
                user.twitter.username    = profile.username;
                user.twitter.displayName = profile.displayName;

                user.save(function(err) {
                    if (err)
                        throw err;
                    return done(null, user);
                });
            }

        });

    }));

    // =========================================================================
    // GOOGLE ==================================================================
    // =========================================================================
    passport.use(new GoogleStrategy({

        clientID        : configAuth.googleAuth.clientID,
        clientSecret    : configAuth.googleAuth.clientSecret,
        callbackURL     : configAuth.googleAuth.callbackURL,
        passReqToCallback : true 
    },
    function(req, token, refreshToken, profile, done) {

        process.nextTick(function() {

            if (!req.user) {

                User.findOne({ 'google.id' : profile.id }, function(err, user) {
                    if (err)
                        return done(err);

                    if (user) {

                        if (!user.google.token) {
                            user.google.token = token;
                            user.google.name  = profile.displayName;
                            user.google.email = profile.emails[0].value; // pull the first email

                            user.save(function(err) {
                                if (err)
                                    throw err;
                                return done(null, user);
                            });
                        }

                        return done(null, user);
                    } else {
                        var newUser          = new User();

                        newUser.google.id    = profile.id;
                        newUser.google.token = token;
                        newUser.google.name  = profile.displayName;
                        newUser.google.email = profile.emails[0].value; // pull the first email

                        newUser.save(function(err) {
                            if (err)
                                throw err;
                            return done(null, newUser);
                        });
                    }
                });

            } else {
                var user               = req.user; // pull the user out of the session

                user.google.id    = profile.id;
                user.google.token = token;
                user.google.name  = profile.displayName;
                user.google.email = profile.emails[0].value; // pull the first email

                user.save(function(err) {
                    if (err)
                        throw err;
                    return done(null, user);
                });

            }

        });

    }));

*/
};
