var Cart = require('./model'),
	Meal = require('../meal/model'),
	Restaurant = require('../restaurant/model'),
	_ = require('underscore'),
	timezoner = require('timezoner'),
	cc = require('currency-codes'),
	async = require('async'),
	mongoose = require('mongoose')


var itemPerPage = 20;


module.exports = function () {
	
	return {

		add: function(req, res, next){
			Cart.findOneAndUpdate({user: req.user.id}, {$addToSet:  {items: mongoose.Types.ObjectId(req.body.meal)}, lastUpdated: Date.now()}, {upsert: true}, function(err, cart){
				next(err);
			});
		},
		fetch: function(req, res, next){
			var page = req.query.page ? req.query.page : 0;

			Cart.findOne({user: req.user.id})
				.populate('items', 'restaurant name priceInLocalCurrency priceInLocalCurrencyString productImage priceInUSD availableDate availableEndDate type')
				.select('items')
				.exec(function(err, cart){
					if (err) {next(err);}
					else{
						Cart.populate(cart, {path: 'items.restaurant', model: 'Restaurant', select: 'restaurantName currency'}, function(err, cart) {
							if (err) {next(err);}
							res.send(cart.items);
						});
					}
				});

		},
		delete: function(req, res, next){
			Cart.findOneAndUpdate({user: req.user.id}, {$pull: mongoose.Types.ObjectId(req.query.id)}, function(err, cart){
				next(err);
			});
		},
		purchase: function(req, res, next){
			
			Cart.findOne({user: req.user.id}, function(err, cart){
				if (err) {next(err);}
				else{
					async.each(cart.items, function(err, item){
						
					});
				}
			});

		}

	}

}