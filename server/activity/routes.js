module.exports = function(app, auth){

	var activity = require('./controller')();

	app.route('/userActivity')
		.get(auth.isLoggedIn, activity.fetchUserActivities);

	app.route('/activity')
		.post(auth.isLoggedIn, activity.create)
		.delete(auth.isLoggedIn, activity.deleteSubActivity)
		.get(auth.isLoggedIn, activity.fetch)
		.put(auth.isLoggedIn, activity.update)
	
	app.route('/post')
		.delete(auth.isLoggedIn, activity.deleteParentActivity);

	app.route('/activityAttributes')
		.get(auth.isLoggedIn, activity.fetchAttributedActivity)

	app.route('/ordercomment')
		.post(auth.isLoggedIn, activity.ordercomment);

	app.route('/restaurantfeed')
		.get(activity.fetchRestaurantFeed);

	app.route('/postcomment')
		.get(auth.isLoggedIn, activity.postcomment);
		
}