var bcrypt = require('bcrypt'),
  uuid = require('node-uuid'),
  validator = require('validator'), 
  User = require('./models/user'),
  crypt = require('crypto'),
  _ = require('underscore'),
  algorithm = 'aes-256-cbc';


var user_cache = {};
var register_callback = null;

function encryptPassword(password, callback) {
  bcrypt.genSalt(10, function(err, salt) {
    if (err) {
      return callback(err);
    }
    bcrypt.hash(password, salt, function(err, hash) {
      return callback(err, hash);
    });
  });
}

function comparePassword(password, hash, callback) {
  console.log("Comparing ", password, " to hash ", hash);
  bcrypt.compare(password, hash, function(err, match) {
    if (err) {
      return callback(err);
    } else {
      return callback(null, match);
    }
  });
}

module.exports = function(app, passport) {
  function clean_user(user) {
    delete user['crypted_password'];
    return user;
  }



  // app.post('/register', function(req,res){

  //   var user = req.body;

  //   if (!validator.isEmail(user.email)) {
  //     return res.status(400).send("Invalid email address");
  //   }
  //   if (!validator.isLength(user.name, 3)) {
  //     return res.status(400).send("Name must be at least 3 characters");
  //   }
  //   if (!validator.isLength(user.password, 3)) {
  //     return res.status(400).send("Password must be at least 3 characters");
  //   }


  // });

  function register(req, res, next) {
    var user = req.body;

    if (!validator.isEmail(user.email)) {
      return res.status(400).send("Invalid email address");
    }
    if (!validator.isLength(user.name, 3)) {
      return res.status(400).send("Name must be at least 3 characters");
    }
    if (!validator.isLength(user.password, 3)) {
      return res.status(400).send("Password must be at least 3 characters");
    }



  // Model.find({}, function (err, docs) {
  //   if (err) return next(err);
  // })

    
    /*
    new models.User({
      email: user.email
    }).fetch().then(function(model) {
      if (model) {
        return next(Error("That email is already registered"));
      } else {
        encryptPassword(user.password, function(err, hash) {
          if (err) return next(err);

          user.token = uuid.v4();
          user.crypted_password = hash;

          delete user['password'];
          delete user['password2'];

          new models.User(user).save().then(function(model) {
            if (register_callback) {
              register_callback(model);
            }
            res.json(clean_user(model.attributes));

          }).catch(next);
        });
      }
    });

    */
  }

  function login(req, res, next) {
    var user = req.body;

/*
    new models.User({
      email: user.email
    }).fetch().then(function(model) {
      if (!model) {
        return res.status(401).send("Invalid credentials");
      }

      console.log("Compare user ", user, " to model ", model.attributes);

      comparePassword(user.password, model.get("crypted_password"), function(err, match) {
        if (err) {
          console.log(err);
          return res.status(401).send("Invalid Credentials");
        }
        if (match) {
          model.token = uuid.v4();

          model.save().then(function(model) {
            res.json(clean_user(model.attributes));

          }).catch(next);

        } else {
          // Passwords don't match
          return res.status(401).send("Invalid Credentials");
        }
      });
    });

    */
  }

  function on_register(callback) {
    register_callback = callback;
  }

  function authenticate(req, res, next) {
    var token = req.headers['authorization'];
    if (token) {
      token = token.split(' ')[1];
    } else {
      token = req.query.token;
      delete req.query.token;
    }

    if (token in user_cache) {
      req.user = user_cache[token];
      next();
    } else {
/*
      new models.User({
        token: token
      }).fetch().then(function(model) {
        if (model) {
          user_cache[token] = model;
          req.user = model;
          return next();
        } else {
          console.log("Invalid token, returning 401");
          return res.status(401).send("Invalid token");
        }
      });

      */
    }
  }

  function clear_leaders(req, res, next) {
    user_cache = {};
    // models.clear_leaders(req, res, next);
    return ;
  }

  function require_admin(req, res, next) {
    if (!req.user.get('is_admin')) {
      res.status(401).send("Unauthorized");
    } else {
      return next();
    }
  }


  app.get('/api/profile',isLoggedIn, function(req, res){

    var user = {};

    user.email = req.user.local.email;
    user._id = req.user._id;
    user.accessToken = req.user.accessToken;
    user.address = req.user.address;
    user.phoneNumber = _.pluck(req.user.phoneNumber, 'number');
    user.isMerchant = req.user.isMerchant;
    user.name = req.user.name;
    user.lastName = req.user.lastName;
    user.address = req.user.address;
    user.profilePic = req.user.profilePic;
    
    res.send(user);
    
  });


  app.post('/login', passport.authenticate('local-login', {
    successRedirect : '/profile', 
    failureRedirect : '/login', 
    failureFlash : true 
  }));

  // process the login form
  app.post('/loginToken', passport.authenticate('local-login-token', {
    successRedirect : '/api/profile', 
    failureRedirect : '/tokenFail', 
    failureFlash : true
  }));

  // SIGNUP =================================
  app.get('/signup', function(req, res) {
    res.render('signup.ejs', { message: req.flash('signupMessage')});
  });

  app.get('/api/signup', function(req, res) {

    var flashMsg =  req.flash('loginMessage')[0];
    console.log(req.flash('loginMessage'));
    res.status(400).send({ message: flashMsg });
  });

  app.post('/api/signup', passport.authenticate('local-signup', {
    successRedirect : '/api/profile',
    failureRedirect : '/api/signup',
    failureFlash : true 
  }));


  app.post('/api/login', passport.authenticate('local-login', {
    successRedirect : '/api/profile',
    failureRedirect : '/api/signup',
    failureFlash : true 
  }));


  // process the signup form
  app.post('/signup', passport.authenticate('local-signup', {
    successRedirect : '/profile',
    failureRedirect : '/api/signup',
    failureFlash : true 
  }));

  app.get('/api/logout', isLoggedIn, function(req, res){

    if (req.session.authenticated) {
        // this destroys the current session (not really necessary because you get a new one
        console.log('authenticated')
        req.session.destroy(function() {
            // if you don't want destroy the whole session, because you anyway get a new one you also could just change the flags and remove the private informations
            // req.session.user.save(callback(err, user)) // didn't checked this
            delete req.session.user;  // remove credentials
            req.session.authenticated = false; // set flag
            res.clearCookie('connect.sid', { path: '/' }); // see comments above                res.send('removed session', 200); // tell the client everything went well
        });
    } else {

    	req.session = null;
  		req.logOut();
	    res.sendStatus(200);

        // res.send('cant remove public session', 500); // public sessions don't containt sensible information so we leave them
    }


  });

	app.get('/logout', function(req, res){
    req.logout();
    res.redirect('/');

  });


  function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()){
      req.authenticated = true; 
    }
    else{
      req.authenticated = false;
    }

    return next();
  }


  return {
    register: register,
    login: login,
    require_admin: require_admin,
    on_register: on_register,
    authenticate: authenticate,
    clear_leaders: clear_leaders,
    isLoggedIn: isLoggedIn
  }
}

