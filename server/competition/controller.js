var _ = require('underscore'),
	async = require('async'),
	Competition = require('./model'),
	CompetitionEntry = require('../competitionEntry/model')




module.exports = function () {
	
	return {

		competition: function(req, res, next, id){
			
			Competition.findById(id, function(err, comp){
				if (err) {next(err);}
				if (!comp) {next(new Error("NO_SUCH_COMPETITION"));}
				req.competition = comp;
				next();
			});

		},
		all: function(req, res, next){

			Competition.find().lean().exec(function(err, competitions){
				if (err) {next(err);}
				else{
					res.send(competitions);
				}
			});
			
		},
		create: function(req, res, next){
		
			var comp = new Competition(req.body);

			comp.save(function(err){
				if (err) {next(err);}
				else{
					res.sendStatus(200);
				}
			});

		},
		userDismissed: function(req, res, next){
			Competition.update({'_id': req.params.competitionId}, {$addToSet: {dismissedUsers: req.user.id}}, function(err){
				if (err) {next(err);}
				else{
					res.sendStatus(200);
				}
			});
		},
		fetchUnvotedCompetition: function(req, res, next){

			Competition.find({$and: [{createDate: {$gte: new Date()}}, {endDate: {$lt: new Date()} }], voters: {$nin: [req.user]}})
						.lean()
						.sort('-createDate')
						.limit(1)
						.exec(function(err, competition){
							if (err) {next(err);}
							if (!competition) {res.sendStatus(200);}
							else{
								res.send(competition[0]);

							}
						});
		},
		fetchUnjoinedCompetition: function(req, res, next){

			Competition.aggregate([
					{
						$lookup: {
							from: "competitionentries",
							foreignField: 'competition',
							localField: '_id',
							as: 'competitors'
						}
					},
					{
						$project:{
							competitors: {
								$filter: {
									input: '$competitors',
									as: 'competitors',
									cond: {$eq: ['$$competitors.user', req.user._id]}
								}
							},
							dismissedUsers:{
								$filter: {
									input: '$dismissedUsers',
									as: 'dismissedUsers',
									cond: {$in: ['$dismissedusers', req.user._id]}
								}
							},
							createDate: 1,
							endDate: 1,
							dismissedUsers: 1,
							competitionTitle: 1,
							competitionDescription: 1
						}

					},
					{
						$project:{
							competitorsCount: {$size:  "$competitors"},
							dismissedUsersCount: {$size: "$dismissedUsers"},
							createDate: 1,
							endDate: 1,
							dismissedUsers: 1,
							competitionTitle: 1,
							competitionDescription: 1

						}
					},
					{
						$match: {
							competitorsCount: {$eq: 0},
							dismissedUsersCount: {$eq: 0}
						}

					},
					{
						$sort: {"createAt":-1}
					},
					{
						$limit: 1
					}

				], function(err, results){
					if (err) {return next(err);}
					if (!results) {return res.sendStatus(200);}
					else{
						res.send(results[0]);
					}
			});

		}


	}

}