module.exports = function (app, auth) {


	var controller = require('./controller')();

	app.route('/crashreport')
		.post(controller.create)
		.get(auth.isLoggedIn, controller.fetch)

}