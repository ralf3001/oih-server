var Order = require('./model'),
	_ = require('underscore'),
	async = require('async'),
	braintree = require("braintree"),
	User = require('../models/user'),
	apn = require('apn'),
	PushToken = require('../models/token'),
	Address = require('../address/model'),
	mongoose = require('mongoose'),
	Receipt = require('../receipt/model'),
	CronJob = require('cron').CronJob,
	Restaurant = require('../restaurant/model')

	apnOptions = {
		cert:process.env.apn_cert, 
		key:process.env.apn_key, 
		passphrase: process.env.apn_passphrase
	};

	gateway = braintree.connect({
	  environment: braintree.Environment.Sandbox,
	  merchantId: process.env.braintreeMerchantId,
	  publicKey: process.env.braintreePublicKey,
	  privateKey: process.env.braintreePrivateKey
	});


module.exports = function () {
	// body...
	var pageLimit = 15;


	return {

		create: function(req, res, next){


			var order = new Order(req.body);

			order.orderUser = req.user;
			order.orderMeal = req.meal;
			order.orderReceipt = req.receipt;
			order.sellerRestaurant = req.restaurant;
			order.orderAmount = Number(req.receipt.totalAmount);
			order.totalChargeInString = req.receipt.totalAmount;
			
			async.waterfall([

				function(callback){

					if (req.body.friend) {
						User.findById(req.body.friend).select('id').exec(function(err, friend){
							if (err) {callback(err);}
							if (!friend) {callback(new Error("Can't find user"));}
							else{
								order.recipientUser = friend;
								callback(null);
							}
						});
					}else{
						order.recipientUser = req.user;
						callback(null);
					}

				},
				function(callback){
					if (req.body.friend) {

						Address.findOne({user: req.body.friend}, function(err, address){
							if (err) {callback(err);}
							else if(!address){callback(new Error("USER_HAS_NO_ADDRESS"));}
							else{
								order.recipientAddress = address;
								callback(null);
							}
						});
					}else{
						Address.findById(req.body.addressId).exec(function(err, address){
							if (err) {callback(err);}
							else if(!address){callback(new Error("USER_HAS_NO_ADDRESS"));}
							else{
								order.recipientAddress = address;
								callback(null);
							}
						});						
					}
				},
				function(callback){

					order.save(function(err){

						if (err) {callback(err);}
						else{

							req.order = order;
							var receipt = req.receipt;
							receipt.save(function(err, receipt){
								callback(err);
							});

						}
					});

				},
				function(callback){


					if (order.orderUser != order.recipientUser){

						fetchPushTokens(order.recipientUser, 'ios', function(err, tokens){
							if (err) {callback(err);}

							process.nextTick(function(){
								_.each(tokens, function(tokenDocument){

									var device = new apn.Device(tokenDocument.token)
									var apnConnection = new apn.Connection(apnOptions);

									var note = new apn.Notification();

									note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
									note.sound = "ping.aiff";
									note.alert = order.orderUser.name + " bought you a " + order.orderMeal.name;
									note.payload = {"message": "com.ralfcheung.OIH.UserDidReceiveANewOrder"};
									apnConnection.pushNotification(note, device);

								});
								callback(null);
							});
						});

					}else{
						callback(null);
					}

				}

			], function(err){
				if (err) { next(err);}
				else{
					next();
					// res.sendStatus(200);
				}
			});

		},
		order: function(req, res, next, id){
			Order.findById(id).populate('sellerRestaurant orderMeal orderUser recipientUser').exec(function(err ,meal){
				if (err) {next(err);}
				if (!order) {next(new Error('Fail to retrieve order'))}
				req.order = order;
				next();
			});

		},
		orderDetail: function(req, res, next){
			res.send(req.order);
		},
		user: function(req, res, next){

/*
			The cursor.skip() method is often expensive because 
			it requires the server to walk from 
			the beginning of the collection or 
			index to get the offset or skip position before beginning to 
			return result. As offset (e.g. pageNumber above) 
			increases, cursor.skip() will become slower and more CPU intensive. 
			With larger collections, cursor.skip() may become IO bound.
*/
			
			var oDate = req.query.orderDate ? req.query.orderDate : Date.now();

			// Order.find({orderUser: req.params.userId, orderDate: {$lte: oDate}}).limit(pageLimit).populate('sellerRestaurant orderMeal orderUser recipientUser').sort('-orderDate').lean().exec(function(err, orders){

			Order.find({$or: [{orderUser: req.query.userId}, {recipientUser: req.query.userId}], orderDate: {$lt: oDate}})
			.limit(pageLimit)
			.populate('sellerRestaurant orderMeal orderUser recipientUser recipientAddress orderReview orderReceipt', 'currency totalAmount userEnteredAddress formattedAddress name restaurantName priceInLocalCurrency priceInLocalCurrencyString type productImage')
			.lean()
			.sort('-orderDate').exec(function(err, orders){
				if (err) {next(err);}
				if (!orders) {next(new Error('Fail to find orders'));}
				res.send(orders);
			});

		},

		restaurantOrders: function(req, res, next){

			var date = req.query.orderDate ? req.query.orderDate : Date.now();
			var restaurants = req.query.restaurant;

			if (restaurants) {

				Order.find({sellerRestaurant: {$in:restaurants}, orderDate: {$lt: date}})
				.populate('recipientUser orderMeal orderUser sellerRestaurant recipientAddress recipientAddress orderReceipt', 'currency totalAmount address name restaurantName userEnteredAddress formattedAddress')
				.limit(pageLimit)
				.lean()
				.sort('-orderDate').exec(function(err, orders){
					if (err) {next(err);}
					res.send(orders);
				});

			}else{

				Restaurant.find({"accountsAssociatedTo": {$in: [req.user.id]}})
							.lean()
							.exec(function(err, restaurants){
								
					if (err) {next(err);}
					else{

						var ids = _.pluck(restaurants, "_id");

						Order.find({sellerRestaurant: {$in:ids}, orderDate: {$lt: date}})
						.populate('recipientUser orderMeal orderUser sellerRestaurant recipientAddress recipientAddress orderReceipt', 'currency totalAmount address name restaurantName userEnteredAddress formattedAddress')
						.limit(pageLimit)
						.lean()
						.sort('-orderDate').exec(function(err, orders){
							if (err) {next(err);}
							res.send(orders);
						});



					}
				});


			}

		},
		orderStatus: function(req, res, next){

			Order.findByIdAndUpdate(req.body._id, {$set: {orderStatus: req.body.status}}, {new: true}).populate('recipientUser orderReceipt').exec(function(err, order){
				if (err) {next(err)}
				else{

					async.parallel([
						function(callback){
							if (req.body.status == 'canceled') {
								voidTransaction(order.orderReceipt, function(err){
									callback(err);
								});
							}else if(req.body.status == 'delivered'){
								createTransaction(order.orderReceipt, function(err){
									callback(err);
								})
							}else{
								callback(null)
							}

						},
						function(callback){
							fetchPushTokens(order.recipientUser, 'ios', function(err, tokens){
								if (err) {callback(err);}

								process.nextTick(function(){
									async.each(tokens, function(tokenDocument, cb){

										var device = new apn.Device(tokenDocument.token)
										var apnConnection = new apn.Connection(apnOptions);

										var note = new apn.Notification();

										note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
										note.sound = "ping.aiff";
										note.alert = 'Your order status has been changed to \'' + order.orderStatus.charAt(0).toUpperCase() + order.orderStatus.slice(1) + '\'';
										note.payload = {"message": "com.ralfcheung.OIH.UpdateOrderNotification"};
										apnConnection.pushNotification(note, device);
										cb(null);

									}, function(err){
										callback(err);
									});
								});
							});

						}

						], function(err, results){

						});

				}
			});
		},
		mostRecentOrder: function(req, res, next){
			Order.findOne({orderUser: req.query.userId})
			.populate('recipientUser orderMeal sellerRestaurant ', 'name restaurantName')
			.select('orderMeal sellerRestaurant quantity')
			.lean()
			.sort('-orderDate').exec(function(err, order){
				if (err) {next(err);}
				else{
					res.send(order);
				}
			});
		},
		acknowledgeOrders: function(req, res, next){

			Order.update({recipientUser: req.query.userId}, {$set: {isAcknowledgedByRecipient: true}}, {multi:true}).exec(function(err, orders){
				next(err);
			});

		},
		unAcknowledgedOrders: function(req, res, next){

			Order.count({recipientUser: req.query.userId, isAcknowledgedByRecipient: false}).exec(function(err, c){
				if (err) {next(err);}
				else{
					res.send({"count": c});
				}
			});

		},
		audioNote: function(req, res, next){
			
			Order.findByIdAndUpdate(req.body.orderId, {$set: {audioReview: req.body.audioURL}}, function(err){
				if (err) {next(err);}
				else{
					res.sendStatus(200);
				}
			});

		},
		cancelOrder: function(req, res, next){

			var date = new Date();
			// console.log(date.getDate());
			// console.log(date.setDate(date.getDate() - 5));

			var job = new CronJob({
			  cronTime: '00 00 12 * * 1-7',
			  onTick: function() {

			  	Order.find({orderDate: {$lt: date}, orderStatus: 'ordered'}, {$set: {orderStatus: 'canceled'}}, function(err, orders){
			  		if (err) {next(err);}
			  		else{
			  		
			  			async.each(orders, function(order, cb){
							voidTransaction(order.orderReceipt, function(err){
								cb(err);
							});
			  			}, function(err){
			  				if (err){
			  					next(err);
			  				}else{
			  					res.sendStatus(200);
			  				}
			  			});

			  		}

			  	});
			    
			     // * Runs every weekday (Monday through Friday)
			     // * at 11:30:00 AM. It does not run on Saturday
			     // * or Sunday.
			     
			  },
			  start: true,
			  timeZone: 'Asia/Hong_Kong'
			});
			job.start();

		},
		payOut: function(req, res, next){

			var date = new Date();
			// console.log(date.getDate());
			// console.log(date.setDate(date.getDate() - 5));

			var job = new CronJob({
			  cronTime: '00 00 12 * * 1-7',
			  onTick: function() {

			  	Order.find({orderDate: {$lt: date}, orderStatus: 'ordered'}, {$set: {orderStatus: 'canceled'}}, function(err, orders){
			  		if (err) {next(err);}
			  		else{
			  		
			  			async.each(orders, function(order, cb){
							voidTransaction(order.orderReceipt, function(err){
								cb(err);
							});
			  			}, function(err){
			  				if (err){
			  					next(err);
			  				}else{
			  					res.sendStatus(200);
			  				}
			  			});

			  		}

			  	});
			    
			     // * Runs every weekday (Monday through Friday)
			     // * at 11:30:00 AM. It does not run on Saturday
			     // * or Sunday.
			     
			  },
			  start: true,
			  timeZone: 'Asia/Hong_Kong'
			});
			job.start();



		}
		
	}


}

findTransaction = function(receiptId, callback){
	Receipt.findById(receiptId, function(err, receipt){
		if (err) {callback(err);}
		else{

			gateway.transaction.find(receipt.receiptId, function(err, transaction){
				if (err) {

				}else{
					if (transaction.status == 'authorized') {
						
						gateway.transaction.void(receipt.receiptId, function(err, t){
							if(err){

							}else{
								callback(null);
							}
						});

					}

				}
			});

		}
	});
}

voidTransaction = function(receipt, callback){

	gateway.transaction.find(receipt.receiptId, function(err, transaction){

		if (err) {
			callback(err);
		}else{

			if (transaction.status == 'authorized') {
				
				gateway.transaction.void(receipt.receiptId, function(err, t){
					if(err){
						console.log('void error');
						callback(err);
					}else{
						if (t.success) {
							console.log('success');
							callback(null);
						}else{
							callback(result.message);
						}
					}
				});

			}else{
				console.log('cannot be voided');
				callback(new Error("CANNOT_BE_VOIDED"));
			}

		}
	});
}

refundTransaction = function(orderId, callback){
	Receipt.findOne({order: orderId}, function(err, receipt){
		if (err) {callback(err);}
		else{
			gateway.transaction.refund(receipt.receiptId, function (err, result) {
			
			});

		}
	});

}


createTransaction = function(receipt, callback){

	gateway.transaction.submitForSettlement(receipt.receiptId, function(err, result){
		if (err) {callback(err);}
		else{
			if (result.success) {
				callback(null);
			}else{
				callback(new Error("CANNOT_BE_SETTLED"));

			}
		}
	});


}

fetchPushTokens = function(user, os, callback){

	process.nextTick(function(){

		PushToken.find({"user": (user.id), "os": os}).exec(function(err, tokens){
			callback(err, tokens);
		});

	});

}
