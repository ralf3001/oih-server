var User = require('../models/user'),
async = require('async'),
_ = require('underscore'),
mongoose = require('mongoose'),
PushToken = require('../models/token'),
Promise = require('promise'),
apn = require('apn'),
Mailgun = require('mailgun-js');


module.exports = function(){

	return {

		add: function(req, res, next){
			var user = req.user;
			var userToAdd = req.userToAdd;

			async.waterfall([
				function(cb){
					var query = User.collection.initializeOrderedBulkOp();
					query.find({"_id": req.userToAdd._id}).updateOne({$addToSet: {'friendRequests': mongoose.Types.ObjectId(user._id)}})
					query.find({"_id": user._id}).update({$addToSet: {'pendingFriendRequests': mongoose.Types.ObjectId(userToAdd._id)}})
					query.execute(function(err, u){
						cb(err, u);					
					});				
				},
				function(user, cb){

					var message = req.userToAdd.name + ' has added you as friend';

					sendPushNotifications(req.userToAdd, message, function(err){
						cb(err, userToAdd);
					});

				},
				function(user, cb){

					res.render('friendRequestAcceptedEmail', {friend: user, 
						mailtoAddress: 'support@getshanon.com',  
						mailtoName: 'Shanon Support Team'}, function(err, text){
							if (err) {cb(err);}
							else{
								sendSupportEmail(user, text, user.name + ' has added you as a friend', function(err){
									cb(err);
								});
							}
						});
				},
				function(html, user, cb){
				}


			], function(err){
				if (err) {next(err);}
				else{
					res.sendStatus(200);
				}
			});

			var query = User.collection.initializeOrderedBulkOp();
			query.find({"_id": req.userToAdd._id}).updateOne({$addToSet: {'friendRequests': mongoose.Types.ObjectId(user._id)}})
			query.find({"_id": user._id}).update({$addToSet: {'pendingFriendRequests': mongoose.Types.ObjectId(userToAdd._id)}})
			query.execute(function(err, u){

				if (err) next(err);
				else{ 
					
					fetchPushTokens(req.userToAdd, 'ios', function(err, tokens){
						if (err) {callback(err);}

						process.nextTick(function(){
							async.each(tokens, function(tokenDocument, cb){

								var device = new apn.Device(tokenDocument.token)
								var apnConnection = new apn.Connection(apnOptions);

								var note = new apn.Notification();

								note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
								note.sound = "ping.aiff";
								note.alert = req.userToAdd.name + ' has added you as friend';
								// note.payload = {"message": "com.ralfcheung.OIH.UpdateOrderNotification"};
								apnConnection.pushNotification(note, device);
								cb(null);

							}, function(err){
								if (err) {next(err);}
								else{
									res.sendStatus(200);
								}
							});
						});
					});


				}
			});

		},
		remove: function(req, res, next){

			User.findById(req.query.userId, function(err, friend){
				if (err) {next(err);}
				if (!friend) {next(new Error("no such user"))}
					else{

						var query = User.collection.initializeOrderedBulkOp();
						query.find({"_id": friend._id}).updateOne({$pull: {'friends': mongoose.Types.ObjectId(req.user._id)}})
						query.find({"_id": req.user._id}).updateOne({$pull: {'friends': mongoose.Types.ObjectId(friend._id)}})
						query.execute(function(err, u){

							if (err) next(err);
							else res.sendStatus(200);
						});

					}
				});

		},
		user: function(req, res, next){
			User.findById(req.body.userId).select('pendingFriendRequests friendRequests local.email name allowToBeAddedAsFriend profilePic').exec(function(err, user){
				if (err) {next(err);}
				if (!user) {next(new Error("No such user"))}
					else{
						req.userToAdd = user;
						next();					
					}
				});
		},
		listFriends: function(req, res, next){

			if (req.user.friends.length) {

				var index = _.findIndex(req.user.friends, function(u){
					return u.toString() === req.user.id.toString();
				});

				if (index > -1) {
					req.user.friends.splice(index, 1);	
				}

				userQueryById(req.user.friends, req.user, 0, 50, function(err, friends){
					if (err) {next(err);}
					else{res.send(friends);}
				});
			}else res.sendStatus(200);

		},
		listPending: function(req, res, next){

			var ids = [];
			_.each(req.user.pendingFriendRequests, function(user){
				ids.push(user);
			});
			
			if (ids.length > 0) {
				userQueryById(ids, req.user, 0, 50, function(err, friends){
					if (err) {next(err);}
					else{res.send(friends);}
				});
			}else res.sendStatus(200);

		},
		listRequests: function(req, res, next){

			if (req.query.count == 1) {
				res.send({"count": req.user.friendRequests.length});
			}else{
				
				var ids = [];
				_.each(req.user.friendRequests, function(user){
					ids.push(user);
				});
				if (ids.length > 0) {
					userQueryById(ids, req.user, 0, 50, function(err, friends){
						if (err) {next(err);}
						else{res.send(friends);}
					});
				}else res.sendStatus(200);

			}

		},
		confirmFriend: function(req, res, next){

			var me = req.user;
			var userToAdd = req.query.userId;

			async.waterfall([
				function(cb){
					User.findById(userToAdd, function(err, user){
						if (!user) {cb(new Error("Can't find user"));}
						else cb(null, user);
					});
				},
				function(user, cb){
					var query = User.collection.initializeOrderedBulkOp();
					query.find({"_id": user._id})
					.updateOne(
					{
						$pull: {friendRequests: mongoose.Types.ObjectId(me._id)}, 
						$pull: {pendingFriendRequests: mongoose.Types.ObjectId(me._id)}, 
						$addToSet: {friends: mongoose.Types.ObjectId(me._id)}
					})

					query.find({"_id": me._id})
					.updateOne(
					{
						$pull: {pendingFriendRequests: mongoose.Types.ObjectId(user._id)}, 
						$pull: {friendRequests: mongoose.Types.ObjectId(user._id)}, 
						$addToSet: {friends: mongoose.Types.ObjectId(user._id)}
					})

					query.execute(function(err, u){

						if (err) cb(err);
						else{
							cb(null, user);
						}
					});					
				},
				function(user, cb){
					var message = req.user.name + ' has confirmed your friend request';

					sendPushNotifications(req.userToAdd, message, function(err){
						cb(err, user);
					});

				},
				function(user, cb){

					res.render('friendRequestAcceptedEmail', {friend: user, 
						mailtoAddress: 'support@getshanon.com',  
						mailtoName: 'Shanon Support Team'}, function(err, text){
							if (err) {cb(err);}
							else{
								sendSupportEmail(user, text, user.name + ' has confirmed your friend request', function(err){
									cb(err);
								});
							}
						});

				}
				], function(err){
					if (err) {next(err);}
					else{
						res.sendStatus(200);
					}
				});

			/*
			User.findById(userToAdd, function(err, user){
				if (!user) {next(new Error("Can't find user"));};

				var query = User.collection.initializeOrderedBulkOp();
				query.find({"_id": user._id})
				.updateOne(
				{
					$pull: {friendRequests: mongoose.Types.ObjectId(me._id)}, 
					$pull: {pendingFriendRequests: mongoose.Types.ObjectId(me._id)}, 
					$addToSet: {friends: mongoose.Types.ObjectId(me._id)}
				})
				
				query.find({"_id": me._id})
				.updateOne(
				{
					$pull: {pendingFriendRequests: mongoose.Types.ObjectId(user._id)}, 
					$pull: {friendRequests: mongoose.Types.ObjectId(user._id)}, 
					$addToSet: {friends: mongoose.Types.ObjectId(user._id)}
				})
				
				query.execute(function(err, u){

					if (err) next(err);
					else{

						fetchPushTokens(user, 'ios', function(err, tokens){
							if (err) {callback(err);}

							process.nextTick(function(){
								async.each(tokens, function(tokenDocument, cb){

									var device = new apn.Device(tokenDocument.token)
									var apnConnection = new apn.Connection(apnOptions);

									var note = new apn.Notification();

									note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
									note.sound = "ping.aiff";
									note.alert = req.user.name + ' has confirmed your friend request';
									// note.payload = {"message": "com.ralfcheung.OIH.UpdateOrderNotification"};
									apnConnection.pushNotification(note, device);
									cb(null);

								}, function(err){
									if (err) {next(err);}
									else{
										res.sendStatus(200);
									}
								});
							});
						});
	
					}
				});

			});
			*/

		},
		removePendingRequest: function(req, res, next){

			var me = req.user;
			var userToRemoveFromPending = req.body.userId;

			User.findById(userToRemoveFromPending, function(err, user){
				if (!user) {next(new Error("Can't find user"))};
				console.log(user);
				var query = User.collection.initializeOrderedBulkOp();
				query.find({"_id": user._id})
				.updateOne(
				{
					$pull: {friendRequests: mongoose.Types.ObjectId(me._id)}
				})
				
				query.find({"_id": me._id})
				.updateOne(
				{
					$pull: {pendingFriendRequests: mongoose.Types.ObjectId(user._id)}
				})
				
				query.execute(function(err, u){
					console.log('complete')
					if (err) next(err);
					else res.sendStatus(200);
				});

			});

		},
		checkFriend: function(req, res, next){

			var user = req.user;
			
			process.nextTick(function() {

				var userInfo = {};
				userInfo.isFriend = foundUser ? 1 : 0;
				userInfo.isPendingRequest = pendingRequest ? 1 : 0;
				userInfo.isFollowingUser = followingUser ? 1 : 0;

				res.send(userInfo);

			});

		},
		friendsCount: function(req, res, next){

			var obj = {};

			obj.friendRequests = req.user.friendRequests.length;
			obj.pendingRequests = req.user.pendingFriendRequests.length;
			obj.friends = req.user.friends.length > 0 ? req.user.friends.length - 1 : 0;
			obj.following = req.user.following.length > 0 ? req.user.following.length : 0;
			obj.followers = req.user.followers.length > 0 ? req.user.followers.length : 0;

			res.send(obj);

		},
		follow: function(req, res, next){

			var me = req.user;
			var userToAdd = req.userToAdd;
			console.log(userToAdd);

			var query = User.collection.initializeOrderedBulkOp();
			query.find({"_id": userToAdd._id})
			.updateOne(
			{
				$addToSet: {followers: mongoose.Types.ObjectId(req.user._id)},
			})
			
			query.find({"_id": me._id})
			.updateOne(
			{
				$addToSet: {following: mongoose.Types.ObjectId(userToAdd._id)},
			})

			query.execute(function(err, u){

				if (err) next(err);
				else{
					res.sendStatus(200);
				}
			});
		},
		unfollow: function(req, res, next){

			var me = req.user;
			var userToUnfollow = req.query.userId;

			var query = User.collection.initializeOrderedBulkOp();
			query.find({"_id": me._id})
			.updateOne(
			{
				$pull: {following: mongoose.Types.ObjectId(userToUnfollow)}, 
			})
			
			query.find({"_id": userToUnfollow})
			.updateOne(
			{
				$pull: {followers: mongoose.Types.ObjectId(me._id)}, 
			})

			query.execute(function(err, u){

				if (err) next(err);
				else{
					res.sendStatus(200);
				}
			});

		},
		fetchFollower: function(req, res, next){
			userQueryById(req.user.followers, req.user, req.query.page, req.query.limit, function(err, data){
				if (err) {next(err);}
				else{
					res.send(data);
				}
			});
		},
		fetchFollowing: function(req, res, next){
			userQueryById(req.user.following, req.user, req.query.page, req.query.limit, function(err, data){
				if (err) {next(err);}
				else{
					res.send(data);
				}
			});

		}

	}



}


function userRemoveFromFriendRequests(me, user){
	return new Promise(function(fulfill, reject){
		var query = User.collection.initializeOrderedBulkOp();
		query.find({"_id": user.id}).update({$pull: {friendRequests: me}})
		query.find({"_id": user.id}).update({$addToSet: {friends: me}});
		query.execute(function(err, u){
			if (err) reject(err);
			else fulfill();
		});

	});
}


function userQueryById (ids, self, page, pageLimit, callback){
	
	pageLimit = Math.min(pageLimit, 50)

	User.find({_id: {$in: ids}})
	.lean()
	.select('name local.email allowToBeAddedAsFriend profilePic')
	.skip(page * pageLimit)
	.limit(pageLimit)
	.exec(function(err, users){
		if (err){
			callback(err);
		}else{
			checkUserRelationshipInfo(self, users, function(users){
				callback(null, users);
			});
		}
	});

}

fetchPushTokens = function(user, os, callback){

	process.nextTick(function(){

		PushToken.find({"user": (user.id), "os": os}).exec(function(err, tokens){
			callback(err, tokens);
		});

	});

}


function checkUserRelationshipInfo(self, users, callback){

	async.each(users, function(user, cb){
		

		var foundUser = _.find(self.friends, function(friend){
			return friend.toString() === user._id.toString();
		});
		
		var pendingRequest = _.find(self.pendingFriendRequests, function(pending){
			return pending.toString() === user._id.toString();
		});

		var followingUser = _.find(self.following, function(following){
			return following.toString() === user._id.toString();
		});

		var friendRequest = _.find(self.friendRequests, function(friendReq){
			return friendReq.toString() === user._id.toString();
		});

		user.isFriend = foundUser ? 1 : 0;
		user.isPendingRequest = pendingRequest ? 1 : 0;
		user.isFollowingUser = followingUser ? 1 : 0;
		user.isWaitingForConfirmation = friendRequest ? 1 : 0;
		user.email = user.local.email;

		cb(null);

	}, function(err){
		callback(users);

	});

}

function sendPushNotifications(user, alertMessage, cb){

	fetchPushTokens(req.userToAdd, 'ios', function(err, tokens){
	if (err) {callback(err);}
	else{
		async.each(tokens, function(tokenDocument, cb){

			var device = new apn.Device(tokenDocument.token)
			var apnConnection = new apn.Connection(apnOptions);

			var note = new apn.Notification();

			note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
			note.sound = "ping.aiff";
			note.alert = alertMessage;
			// note.payload = {"message": "com.ralfcheung.OIH.UpdateOrderNotification"};
			apnConnection.pushNotification(note, device);
			cb(null);

		}, function(err){
			cb(err, req.userToAdd);
		});
		
	}
});

}


function sendSupportEmail (user, html, subject, callback){

	var data = {
		    //Specify email data
		    from: 'Shanon <support@getshanon.com>',
		    //The email to contact
		    to: user.local.email,
		    //Subject and text data  
		    subject: subject,
		    html: html
		}

    //Invokes the method to send emails given the above data with the helper library
    var mailgun = new Mailgun({apiKey: process.env.mailgun_API_key, domain: "getshanon.com"});

    mailgun.messages().send(data, function (err, body) {
        //If there is an error, render the error page
        if (err) {callback(err);}
        else{
        	callback(null);
        }

    });

}

