module.exports = function (app, auth) {


	var address = require('./controller')();

	app.route('/address')
		.post(auth.isLoggedIn, address.addAddress)
		.get(auth.isLoggedIn, address.fetchAddress)
		.delete(auth.isLoggedIn, address.delete);

	app.route('/testadd')
		.post(address.test);

	// app.param('restaurantId', restaurants.restaurant);

}