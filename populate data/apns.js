var apn = require('apn');

var token = '6f4f190029df7d98c2565fec1cdabe435c903a34de248c2aa75f4ee04da1404e'

var options = {cert:process.env.apn_cert, key:process.env.apn_ket, passphrase: process.env.apn_passphrase};
var device = new apn.Device(token)
var apnConnection = new apn.Connection(options);

var note = new apn.Notification();
note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
note.badge = 3;
note.sound = "ping.aiff";
note.alert = "\uD83D\uDCE7 \u2709 You have a new message";
note.payload = {'messageFrom': 'Caroline'};

apnConnection.pushNotification(note, device);
