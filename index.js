var http	 = require('http');
var express  = require('express');
var app      = express();
var port     = process.env.PORT || 3000;
var cookieParser = require("cookie-parser");
var bodyParser = require("body-parser");
var passport = require('passport');
var session = require('express-session');
var mongoose = require('mongoose');
var logger = require('morgan');
var compression = require('compression');
var flash    = require('connect-flash');
var cookieSession = require('cookie-session');
var scribe = require('scribe-js')(),
	cluster = require('cluster'),
	redis = require('redis'),
	env = require('node-env-file');

	// Ddos = require('ddos'),
	// ddos = new Ddos;



if ((cluster.isMaster) &&
  (process.execArgv.indexOf('--debug') < 0) &&
  (process.env.NODE_ENV!=='test') && (process.env.NODE_ENV!=='development') &&
  (process.execArgv.indexOf('--singleProcess')<0)) {
//if (cluster.isMaster) {

    console.log('for real!');
    // Count the machine's CPUs
    var cpuCount = require('os').cpus().length;

    // Create a worker for each CPU
    for (var i = 0; i < cpuCount; i += 1) {
        console.log ('forking ',i);
        cluster.fork();
    }

    // Listen for dying workers
    cluster.on('exit', function (worker) {
        // Replace the dead worker, we're not sentimental
        console.log('Worker ' + worker.id + ' died :(');
        cluster.fork();

    });

// Code to run if we're in a worker process
}else{
	
	var workerId = 0;
    if (!cluster.isMaster){
        workerId = cluster.worker.id;
    }

	if (process.env.NODE_ENV === "production") {
	    env(__dirname + '/env/.env2');
	}else env(__dirname + '/env/.env');


// Creates and serves mean application
	require('mongoose-cache').install(mongoose, {max: 50, maxAge: 1000*60*2})

	// app.use(ddos.express)
	app.use(compression());
	app.use(cookieSession({keys: ['ncie0fnft6wjfmgtjz8i']}));
	app.use(scribe.express.logger());

	app.use(passport.initialize());
	app.use(passport.session()); // persistent login sessions

	app.use(bodyParser.json());
	app.use(bodyParser.urlencoded({ extended: true, limit: '7mb',parameterLimit: 100000 }));


	// app.use(express.static(path.join(__dirname, 'public')));
	app.set('views', __dirname + '/views/js');
	app.set('view engine', 'jade'); // set up ejs for templating
	app.use(cookieParser()); // read cookies (needed for auth)
	app.use(flash()); // use connect-flash for flash messages stored in session
	app.use(function(req, res, next) {
	    res.setHeader('Access-Control-Allow-Origin', '*');
	    res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
	    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
	    next();
	});
	

	require('./config/passport')(passport);

	var auth = require('./server/auth')(app, passport);
	require('./server/food')(app, auth);

	require('./server/payment/routes')(app, auth);
	require('./server/restaurant/routes')(app, auth);
	require('./server/meal/routes')(app, auth);
	require('./server/order/routes')(app, auth);
	require('./server/friend/routes')(app, auth);
	require('./server/user/routes')(app, auth);
	require('./server/activity/routes')(app, auth);
	require('./server/address/routes')(app, auth);
	require('./server/eventLog/routes')(app, auth);
	require('./server/conversation/routes')(app, auth);
	require('./server/conversationMessage/routes')(app, auth);
	require('./server/crashReport/routes')(app, auth);
	require('./server/competition/routes')(app, auth);
	require('./server/competitionEntry/routes')(app, auth);
	require('./server/subscription/routes')(app, auth);
	require('./server/cart/routes')(app, auth);
	require('./server/searchResults/routes')(app, auth);


	var ex = require('./server/exchange-rate/controller')();
	ex.fetch();
	
	app.use('/logs', scribe.webPanel());

	var server = http.createServer(app).listen(port, function(){
	  console.log("HTTP listening on port", port);
	});

	var sio = require('socket.io')(server);
	var RedisStore = sio.RedisStore;
	var adapter = require('socket.io-redis');
	// io = sio.listen();

	// var redis = require('socket.io-redis');
	// var adapter = redis('localhost:6379');

	// adapter.pubClient.on('error', function(){
	// 	console.log('pub falied');
	// });
	// adapter.subClient.on('error', function(){
	// 	console.log('sub failed');
	// });

	var pub = redis.createClient(6379, 'localhost');
	pub.auth('cky7796', function(err){if (err) throw err;});

	var sub = redis.createClient(6379, 'localhost');
	sub.auth('cky7796', function(err){if (err) {throw err;}});

	sio.adapter(adapter({pubClient: pub, subClient: sub}));
	// io.adapter(new RedisStore({ pubClient: pub, subClient: sub }));

    // io.adapter(redis({ host: 'localhost', port: 6379 }));


    sio
    .of('/namespace')
    .on('connection', function(socket){

	    console.log('a user connected with id %s', socket.id);

	    socket.on('message', function (data) {
	        io.of('namespace').emit('my-message', data);
	        console.log('broadcasting my-message', data);
	    });

    });

	sio.on('connection', function(socket){
		console.log('connected');
	  socket.on('event', function(data){console.log(data)});
	  socket.on('disconnect', function(){console.log('disconnected')});
	  socket.on('testing', function(data){console.log(data)});

		setInterval(function(str1, str2) {
			console.log('broadcasting')
			socket.emit('msg', 'test message');
		}, 1000);

	});


}


