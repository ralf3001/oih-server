var braintree = require("braintree"),
	async = require('async'),
	Mailgun = require('mailgun-js'),

	paypal = require('paypal-rest-sdk')

	paypal.configure({
	  'mode': 'sandbox', //sandbox or live
	  'client_id': 'AXSVxneP8mUHKxNqoUcvA7j6VupexuBQVHQDuiYf56qEZNMqZDuyb9BO9llJUO6cLvx-VXnWeeQelC25',
	  'client_secret': 'EDBNnfHuNR_9g5zlltFx80zWiObqEyjR2xNW1Zc1B2f0jz3otTPBTl1hHuXJ70jchifzjQZ7eCjm7aQ5'
	});

	gateway = braintree.connect({
	  environment: braintree.Environment.Sandbox,
	  merchantId: process.env.braintreeMerchantId,
	  publicKey: process.env.braintreePublicKey,
	  privateKey: process.env.braintreePrivateKey
	});


module.exports = function(){

	return{

		clientToken: function(req, res){
			
			gateway.clientToken.generate({
			  customerId: req.user.braintreeCustomerId
			}, function (err, response) {
				if (err) {next(err);}
				else res.send(response);

			});
		},
		meal: function(req, res, next){
			var mealId = req.body.mealId;
			// async
		},
		create: function(req, res, next, id){
			
			var user = req.user;

			gateway.transaction.sale({
				amount: req.meal.priceInLocalCurrency,
				billing:{
					countryName: req.restaurant.address.country
				},
				customer:{
					id: req.user.braintreeCustomerId
				},
				merchantAccountId: req.restaurant.braintreeMerchantAccountId,
				paymentMethodNonce: nonceFromTheClient,
				options: {
					submitForSettlement: true,
					holdInEscrow: true,
				},

				serviceFeeAmount: req.meal.priceInLocalCurrency * 0.2
			}, function (err, result) {

				if (err) {next(err)}
				else{next();}

			});

		},

		userPayments: function(req, res, next){

			var sender_batch_id = Math.random().toString(36).substring(9);

			var create_payout_json = {
			    "sender_batch_header": {
			        "sender_batch_id": sender_batch_id,
			        "email_subject": "You have a payment"
			    },

			    "items": [
			        {
			            "recipient_type": "EMAIL",
			            "amount": {
			                "value": 0.99,
			                "currency": "USD"
			            },
			            "receiver": "shirt-supplier-one@mail.com",
			            "note": "Thank you.",
			            "sender_item_id": "item_1"
			        },
			        {
			            "recipient_type": "EMAIL",
			            "amount": {
			                "value": 0.90,
			                "currency": "USD"
			            },
			            "receiver": "shirt-supplier-two@mail.com",
			            "note": "Thank you.",
			            "sender_item_id": "item_2"
			        },
			        {
			            "recipient_type": "EMAIL",
			            "amount": {
			                "value": 2.00,
			                "currency": "USD"
			            },
			            "receiver": "shirt-supplier-three@mail.com",
			            "note": "Thank you.",
			            "sender_item_id": "item_3"
			        }
			    ]

			}

			paypal.payout.create(create_payout_json, function (error, payout) {
			    if (error) {
			        console.log(error.response);
			        throw error;
			    } else {
			        console.log("Create Payout Response");
			        console.log(payout);
			    }
			});

		},
		monthlySales: function(req, res, next){
			userRestaurantMonthlySales(req, function(err, result){

				var total = 0;
				_.each(result, function(restaurant){
					total += restaurant.value;
				});

				req.amount = total;

				next();
			});
		},
		userPayout: function(req, res, next){

			var sender_batch_id = Math.random().toString(36).substring(9);

			var account = _.find(req.braintreeInfo.paypalAccounts, function(paypal){
				return paypal.default === true;
			});

			if (!account) {
				account = req.braintreeInfo.paypalAccounts[0];
			}
			var amount = parseFloat(Math.round(req.amount * 100) / 100).toFixed(2);

			var create_payout_json = {
			    "sender_batch_header": {
			        "sender_batch_id": sender_batch_id,
			        "email_subject": "You have a payment"
			    },
			    "items": [
			        {
			            "recipient_type": "EMAIL",
			            "amount": {
			                "value": amount,
			                "currency": req.currency
			            },
			            "receiver": account.email,
			            "note": "Thank you.",
			            "sender_item_id": "item_1"
			        },
				]
			};

			paypal.payout.create(create_payout_json, function (error, payout) {
			    if (error) {
			        next(error);
			    } else {
			        console.log("Create Payout Response");
			        sendPayoutEmail(payout, req.braintreeInfo.email, function(err){
			        	if (err) {next(err);}
			        	else{
					        res.sendStatus(200);
			        	}
			        });
			    }
			});

		}

	};


}

sendPayoutEmail = function(html, email, callback){

	console.log(email)

	var data = {
	    //Specify email data
	    from: 'Shanon Payout <support@getshanon.com>',
	    //The email to contact
	    to: email,
	    //Subject and text data  
	    subject: 'This month\'s payout',
	    html: JSON.stringify(html)
	}

    //Invokes the method to send emails given the above data with the helper library
    var mailgun = new Mailgun({apiKey: process.env.mailgun_API_key, domain: "getshanon.com"});

    mailgun.messages().send(data, function (err, body) {
        //If there is an error, render the error page
        if (err) {callback(err);}
        else{
        	callback(null);
        }

    });



}


userRestaurantMonthlySales = function(req, callback){

	var restaurant = req.restaurants;

	var today = new Date();

	var y = req.query.year ? req.query.year : today.getFullYear();
	var m = req.query.month ? req.query.month : today.getMonth() ;

	var start = new Date(y, m, 1);

	var o = {};
	o.map = function(){
		emit(
				this.sellerRestaurant,
				
			{
				quantity: this.quantity,
				amount: this.orderAmount
			}
		)
	};

	o.reduce = reduceFunction;
	o.out = {replace: 'salesMapReduce'};

	if (restaurant.constructor === Array) {
		o.query = {orderDate:{$gte: start, $lt: new Date(y,m+1,0)}, sellerRestaurant: {$in: restaurant}};

	}else{
		o.query = {orderDate:{$gte: start, $lt: new Date(y,m+1,0)}, sellerRestaurant: restaurant};
	}

	totalSales(o, function(err, result){
		callback(err, result);
	});


}

var mapFunction = function(){
	emit(this.sellerRestaurant, this.orderAmount)
}


var reduceFunction = function(key, values){
	var reduced = 0;
	for (var i = 0; i < values.length; i++) {
		reduced +=  values[i].amount;
		// reduced = Number(values[i].amountInString);
	};
	return reduced;
}


totalSales = function(o, cb){

	Order.mapReduce(o, function(err, data, stats){

		if (err) {console.log('err', err); next(err);}
		else{

			data
			.find()
			.populate({path:'_id', select: 'restaurantName', model: 'Restaurant'})
			.exec(function(err, result){
				if (err) {console.log('err', err); cb(err);}
				else{

					_.each(result, function(restaurant){

						if (restaurant.value.amount) {
							restaurant.value = restaurant.value.amount;
						}
						restaurant.restaurantName = restaurant._id.restaurantName;
						restaurant._id = restaurant._id._id;

					});

					result.sort(function(a, b){
						if (a && b) {
							return (a.value < b.value) ? -1 : (a.value > b.value) ? 1 : 0;
						}
					});

					cb(null, result);
				}

			});

		}
	});


}


// function isLoggedIn(req, res, next) {
// 	if (req.isAuthenticated())
// 		return next();

// 	res.redirect('/');
// }




