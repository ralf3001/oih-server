var mongoose = require('mongoose');
var User = require('../models/user');
var connection = require('../../config/userDBConnection');

var rejectedSchema = mongoose.Schema({

	type: {type: String, enum: ['orderReject'], required: true},
	rate: Number,
	createdDate: {type: Date, default: Date.now},
	geometry: {
		type: {type: String},
		coordinates: []
	},

});

var rejectedModel = connection.model('Event', rejectedSchema);
module.exports = rejectedModel;