db.createRole(
{
    role: "customerService",
    privileges: [
      { resource: { db: "alphaAccounts", collection: "users" }, actions: [ "update", "insert", "remove", "find" ] },
      { resource: { db: "alphaAccounts", collection: "restaurants" }, actions: [ "update", "insert", "remove", "find" ] },
      { resource: { db: "alphaAccounts", collection: "meals" }, actions: [ "update", "insert", "remove" ] },
      { resource: { db: "alphaAccounts", collection: "orders" }, actions: [ "update", "insert", "remove" ] },
      { resource: { db: "alphaAccounts", collection: "receipts" }, actions: [ "find"] },
      { resource: { db: "alphaAccounts", collection: "coupons" }, actions: [ "insert", "find"] },
      { resource: { db: "alphaAccounts", collection: "addresses" }, actions: [ "insert", "find", "remove"] }

    ],
    roles: []
},
  { w: "majority" , wtimeout: 5000 }
);

// db.createRole(
// {
//     role: "customerService",
//     privileges: [
//       { resource: { cluster: true }, actions: [ "addShard" ] },
//       { resource: { db: "alphaAccounts", collection: "users" }, actions: [ "update", "insert", "remove" ] },
//       { resource: { db: "alphaAccounts", collection: "restaurants" }, actions: [ "update", "insert", "remove" ] },
//       { resource: { db: "alphaAccounts", collection: "meals" }, actions: [ "update", "insert", "remove" ] },
//       { resource: { db: "alphaAccounts", collection: "orders" }, actions: [ "update", "insert", "remove" ] },
//       { resource: { db: "alphaAccounts", collection: "receipts" }, actions: [ "find"] },

//     ],
//     roles: [
//     { role: "read", db: "admin" }
//     ]
// },
//   { w: "majority" , wtimeout: 5000 }
// )